package com.nic.st;

import com.mojang.serialization.Codec;
import com.nic.st.abilities.*;
import com.nic.st.abilities.conditions.*;
import com.nic.st.blocks.VoxelCreatorBlock;
import com.nic.st.blocks.VoxelPrinterBlock;
import com.nic.st.blocks.tileentity.VoxelCreatorTileEntity;
import com.nic.st.blocks.tileentity.VoxelPrinterTileEntity;
import com.nic.st.items.NanobotWelderItem;
import com.nic.st.items.VoxelItem;
import com.nic.st.network.LoadVoxelMessage;
import com.nic.st.network.SaveVoxelMessage;
import com.nic.st.network.SetColorMessage;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderTypeLookup;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemModelsProperties;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.particles.BlockParticleData;
import net.minecraft.particles.ParticleType;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.fml.network.NetworkDirection;
import net.minecraftforge.fml.network.NetworkRegistry;
import net.minecraftforge.fml.network.simple.SimpleChannel;
import net.minecraftforge.registries.ObjectHolder;
import net.threetag.threecore.ability.AbilityType;
import net.threetag.threecore.ability.condition.ConditionType;
import net.threetag.threecore.util.RenderUtil;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Field;
import java.util.Optional;

@Mod(value = StarTech.MODID)
@Mod.EventBusSubscriber
public class StarTech {
    public static final String MODID = "star-tech";
    public static final String PROTOCOL_VERSION = "1";

    public static SimpleChannel simpleNetworkWrapper;

    public StarTech() {
        // Register the setup method for modloading
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
        // Register the doClientStuff method for modloading
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::doClientStuff);
        // Register ourselves for server and other game events we are interested in
        MinecraftForge.EVENT_BUS.register(this);
    }

    private void setup(final FMLCommonSetupEvent event) {
        simpleNetworkWrapper = NetworkRegistry.newSimpleChannel(new ResourceLocation(MODID, "network"), () -> PROTOCOL_VERSION, PROTOCOL_VERSION::equals, PROTOCOL_VERSION::equals);
        simpleNetworkWrapper.registerMessage(0, SetColorMessage.class, SetColorMessage::toBytes, SetColorMessage::new, SetColorMessage::handle, Optional.of(NetworkDirection.PLAY_TO_SERVER));
        simpleNetworkWrapper.registerMessage(1, SaveVoxelMessage.class, SaveVoxelMessage::toBytes, SaveVoxelMessage::new, SaveVoxelMessage::handle, Optional.of(NetworkDirection.PLAY_TO_SERVER));
        simpleNetworkWrapper.registerMessage(2, LoadVoxelMessage.class, LoadVoxelMessage::toBytes, LoadVoxelMessage::new, LoadVoxelMessage::handle, Optional.of(NetworkDirection.PLAY_TO_SERVER));
    }

    private void doClientStuff(final FMLClientSetupEvent event) {
        RenderTypeLookup.setRenderLayer(Blocks.hologram, RenderUtil.RenderTypes.translucent());
        RenderTypeLookup.setRenderLayer(Blocks.voxel_printer, RenderUtil.RenderTypes.translucent());
        RenderTypeLookup.setRenderLayer(Blocks.voxel_creator, RenderUtil.RenderTypes.translucent());
        ItemModelsProperties.register(Items.nanobot_welder, new ResourceLocation(MODID, "storage"), (stack, world, living) -> {
            CompoundNBT abilities = stack.getOrCreateTag().getCompound("Abilities");
            Optional<String> storageAbilityName = abilities.getAllKeys().stream().filter(s -> s.equals("storage")).findFirst();
            return storageAbilityName.isPresent() && abilities.getCompound(storageAbilityName.get()).getCompound("Data").getCompound("storage").getAllKeys().isEmpty() ? 0.0f : 1.0f;
        });
    }

    @Retention(RetentionPolicy.RUNTIME)
    private @interface NoItem {
    }

    @ObjectHolder(MODID)
    public static class Blocks {
        @NoItem
        public static final Block voxel_creator = null;
        @NoItem
        public static final Block hologram = null;
        @NoItem
        public static final Block voxel_printer = null;
    }

    @ObjectHolder(MODID)
    public static class Items {
        public static final Item voxel_item = null;
        public static final Item nanobot_welder = null;
        public static final Item memory_card = null;
    }

    @ObjectHolder(MODID)
    public static class TileEntityTypes {
        public static final TileEntityType<VoxelCreatorTileEntity> voxel_creator = null;
        public static final TileEntityType<VoxelPrinterTileEntity> voxel_printer = null;
    }

    @ObjectHolder(MODID)
    public static class ParticleTypes {
        public static final ParticleType<BlockParticleData> welder = null;
    }

    public static class AbilityTypes {
        public static AbilityType switch_creator_type = new AbilityType(AbilityVoxelCreator::new).setRegistryName(MODID, "switch_creator_type");
        public static AbilityType switch_material = new AbilityType(AbilitySwitchMaterial::new).setRegistryName(MODID, "switch_material");
        public static AbilityType set_color = new AbilityType(AbilitySetColor::new).setRegistryName(MODID, "set_color");
        public static AbilityType storage = new AbilityType(AbilityVoxelStorage::new).setRegistryName(MODID, "storage");
        public static AbilityType eject_memory_card = new AbilityType(AbilityEjectMemoryCard::new).setRegistryName(MODID, "eject_memory_card");
        public static AbilityType print_voxel = new AbilityType(AbilityVoxelPrint::new).setRegistryName(MODID, "print_voxel");
    }

    public static class ConditionTypes {
        public static ConditionType empty_data = new ConditionType(EmptyDataCondition::new).setRegistryName(MODID, "empty_data");
        public static ConditionType empty_opposite = new ConditionType(EmptyOppositeCondition::new).setRegistryName(MODID, "empty_opposite");
        public static ConditionType has_creator = new ConditionType(HasCreatorCondition::new).setRegistryName(MODID, "has_creator");
        public static ConditionType printer_condition = new ConditionType(PrinterCondition::new).setRegistryName(MODID, "printer_condition");
        public static ConditionType has_printer = new ConditionType(HasPrinterCondition::new).setRegistryName(MODID, "has_printer");
    }

    public static class Sounds {
        public static SoundEvent shoot = null;
    }

    @Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
    public static class RegistryEvents {

        @SubscribeEvent
        public static void registerBlocks(RegistryEvent.Register<Block> event) {
            event.getRegistry().register(new VoxelCreatorBlock().setRegistryName(MODID, "voxel_creator"));
            event.getRegistry().register(new VoxelPrinterBlock().setRegistryName(MODID, "voxel_printer"));
            event.getRegistry().register(new Block(AbstractBlock.Properties.of(VoxelCreatorBlock.HOLOGRAM).noOcclusion()).setRegistryName(MODID, "hologram"));
        }

        @SubscribeEvent
        public static void registerTileEntity(RegistryEvent.Register<TileEntityType<?>> event) {
            event.getRegistry().register(TileEntityType.Builder.of(VoxelCreatorTileEntity::new, Blocks.voxel_creator).build(null).setRegistryName(MODID, "voxel_creator"));
            event.getRegistry().register(TileEntityType.Builder.of(VoxelPrinterTileEntity::new, Blocks.voxel_printer).build(null).setRegistryName(MODID, "voxel_printer"));
        }

//		@SubscribeEvent
//		public static void registerEntityEntry(RegistryEvent.Register<EntityEntry> event)
//		{
//			event.getRegistry().register(
//					EntityEntryBuilder.create().entity(EntityBullet.class)
//							.id(new ResourceLocation(MODID, "bullet"), 0).name("bullet")
//							.tracker(80, 10, true)
//							.build());
//		}

        @SubscribeEvent
        public static void registerConditions(RegistryEvent.Register<ConditionType> event) {
            event.getRegistry().register(ConditionTypes.empty_data);
            event.getRegistry().register(ConditionTypes.empty_opposite);
            event.getRegistry().register(ConditionTypes.has_creator);
            event.getRegistry().register(ConditionTypes.printer_condition);
            event.getRegistry().register(ConditionTypes.has_printer);
        }

        @SubscribeEvent
        public static void registerAbilities(RegistryEvent.Register<AbilityType> event) {
            event.getRegistry().register(AbilityTypes.switch_creator_type);
            event.getRegistry().register(AbilityTypes.switch_material);
            event.getRegistry().register(AbilityTypes.set_color);
            event.getRegistry().register(AbilityTypes.storage);
            event.getRegistry().register(AbilityTypes.eject_memory_card);
            event.getRegistry().register(AbilityTypes.print_voxel);
        }

        @SubscribeEvent
        public static void registerItems(RegistryEvent.Register<Item> event) throws IllegalAccessException {
            event.getRegistry().register(new VoxelItem().setRegistryName(MODID, "voxel_item"));
            event.getRegistry().register(new NanobotWelderItem().setRegistryName(MODID, "nanobot_welder"));
            event.getRegistry().register(new Item(new Item.Properties().stacksTo(1)).setRegistryName(MODID, "memory_card"));

            for (Field field : Blocks.class.getDeclaredFields()) {
                if (field.getAnnotation(NoItem.class) == null) {
                    Block block = (Block) field.get(null);
                    Item itemBlock = new BlockItem(block, new Item.Properties()).setRegistryName(block.getRegistryName());
                    event.getRegistry().register(itemBlock);
                }
            }

        }

        @SubscribeEvent
        public static void registerSounds(RegistryEvent.Register<SoundEvent> event) {
            Sounds.shoot = new SoundEvent(new ResourceLocation(MODID, "shoot")).setRegistryName(MODID, "shoot");
            event.getRegistry().register(Sounds.shoot);
        }

        @SubscribeEvent
        public static void registerParticleTypes(RegistryEvent.Register<ParticleType<?>> event) {
            event.getRegistry().register(new ParticleType<BlockParticleData>(false, BlockParticleData.DESERIALIZER) {
                @Override
                public Codec<BlockParticleData> codec() {
                    return BlockParticleData.codec(this);
                }
            }.setRegistryName(MODID, "welder"));
        }
    }

}
