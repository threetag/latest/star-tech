package com.nic.st.abilities;

import com.nic.st.StarTech;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.LivingEntity;
import net.threetag.threecore.ability.Ability;
import net.threetag.threecore.block.TCBlocks;
import net.threetag.threecore.util.icon.ItemIcon;
import net.threetag.threecore.util.threedata.EnumSync;
import net.threetag.threecore.util.threedata.IntegerThreeData;
import net.threetag.threecore.util.threedata.ThreeData;

public class AbilitySwitchMaterial extends Ability {

    public static final ThreeData<Integer> MATERIAL = new IntegerThreeData("material").setSyncType(EnumSync.SELF);
    public static final BlockState[] POSSIBLE_MATERIALS = new BlockState[]{Blocks.IRON_BLOCK.defaultBlockState(), Blocks.GOLD_BLOCK.defaultBlockState(), Blocks.DIAMOND_BLOCK.defaultBlockState(), Blocks.NETHERITE_BLOCK.defaultBlockState(), TCBlocks.COPPER_BLOCK.get().defaultBlockState(), TCBlocks.LEAD_BLOCK.get().defaultBlockState(), TCBlocks.SILVER_BLOCK.get().defaultBlockState(), TCBlocks.TITANIUM_BLOCK.get().defaultBlockState(), TCBlocks.VIBRANIUM_BLOCK.get().defaultBlockState()};

    public AbilitySwitchMaterial() {
        super(StarTech.AbilityTypes.switch_material);
    }

    @Override
    public void action(LivingEntity entity) {
        if (!entity.level.isClientSide) {
            int material = get(MATERIAL) + 1;
            if (material >= POSSIBLE_MATERIALS.length)
                material = 0;
            set(ICON, new ItemIcon(POSSIBLE_MATERIALS[material].getBlock()));
            set(MATERIAL, material);
        }
    }

    public void registerData() {
        super.registerData();
        this.dataManager.register(MATERIAL, 0);
        this.set(ICON, new ItemIcon(POSSIBLE_MATERIALS[get(MATERIAL)].getBlock()));
    }
}
