package com.nic.st.abilities;

import com.nic.st.StarTech;
import com.nic.st.blocks.tileentity.VoxelPrinterTileEntity;
import com.nic.st.items.VoxelItem;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.LivingEntity;
import net.minecraft.particles.BlockParticleData;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.vector.Vector3d;
import net.threetag.threecore.ability.Ability;
import net.threetag.threecore.util.PlayerUtil;
import net.threetag.threecore.util.threedata.EnumSync;
import net.threetag.threecore.util.threedata.IntegerArrayThreeData;
import net.threetag.threecore.util.threedata.ThreeData;

import java.util.Optional;
import java.util.Random;

public class AbilityVoxelPrint extends Ability {

    public static final ThreeData<Integer[]> PRINTER_POS = new IntegerArrayThreeData("printer").setSyncType(EnumSync.SELF);

    public AbilityVoxelPrint() {
        super(StarTech.AbilityTypes.print_voxel);
    }

    @Override
    public void action(LivingEntity entity) {
        if (!entity.level.isClientSide) {
            RayTraceResult hitVec = entity.pick(5.0f, 0.0f, false);
            if (!(hitVec instanceof BlockRayTraceResult) || hitVec.getType() == RayTraceResult.Type.MISS) return;
            if (getPrinter() == null) {
                BlockPos pos = ((BlockRayTraceResult) hitVec).getBlockPos().relative(((BlockRayTraceResult) hitVec).getDirection());
                if (entity.level.isEmptyBlock(pos)) {
                    placePrinter(pos);
                    this.getConditionManager().disableKeybounds();
                }
            } else {
                double progress = Math.min(0.5, (0.5 + (this.ticks / 20)) * 0.0625);
                BlockPos pos = ((BlockRayTraceResult) hitVec).getBlockPos();
                TileEntity te = entity.level.getBlockEntity(pos);
                if (!(te instanceof VoxelPrinterTileEntity)) return;

                for (int i = 0; i < 3; i++) {
                    Random rand = new Random();
                    int a = Math.min(8, this.ticks / 20);
                    Vector3d endPos = hitVec.getLocation().add((a) * rand.nextFloat() * (rand.nextBoolean() ? -0.04 : 0.04), (a) * rand.nextFloat() * (rand.nextBoolean() ? -0.04 : 0.04), (a) * rand.nextFloat() * (rand.nextBoolean() ? -0.04 : 0.04));
                    Vector3d handVect = calculateViewVector(entity, 90).multiply(0.3f, 0.0f, 0.3f);
                    Vector3d forVect = calculateViewVector(entity, 0).multiply(0.7f, 0.7f, 0.7f);
                    Vector3d startPos = entity.getEyePosition(0.0f).add(handVect).add(forVect).subtract(0, 0.2f, 0);
                    double d = endPos.distanceTo(startPos);
                    PlayerUtil.spawnParticleForAll(entity.level, 50, new BlockParticleData(StarTech.ParticleTypes.welder, Blocks.IRON_BLOCK.defaultBlockState()), false, startPos.x, startPos.y, startPos.z, (float) ((endPos.x - startPos.x) / d), (float) ((endPos.y - startPos.y) / d), (float) ((endPos.z - startPos.z) / d), 0.06f, 0);
                }

                if (!((VoxelPrinterTileEntity) te).print((BlockRayTraceResult) hitVec, progress))
                    this.getConditionManager().disableKeybounds();
            }
        }
    }

    private Vector3d calculateViewVector(LivingEntity entity, float yOffset) {
        float yRot = entity.getViewYRot(0.0f) + yOffset;
        float f = entity.getViewXRot(0.0f) * ((float) Math.PI / 180F);
        float f1 = -yRot * ((float) Math.PI / 180F);
        float f2 = MathHelper.cos(f1);
        float f3 = MathHelper.sin(f1);
        float f4 = MathHelper.cos(f);
        float f5 = MathHelper.sin(f);
        return new Vector3d(f3 * f4, -f5, f2 * f4);
    }

    @Override
    public void registerData() {
        super.registerData();
        this.dataManager.register(PRINTER_POS, new Integer[0]);
    }

    public VoxelPrinterTileEntity getPrinter() {
        if (this.entity == null) return null;
        if (get(PRINTER_POS).length > 1) {
            BlockPos pos = new BlockPos(get(PRINTER_POS)[0], get(PRINTER_POS)[1], get(PRINTER_POS)[2]);
            TileEntity te = this.entity.level.getBlockEntity(pos);
            if (te instanceof VoxelPrinterTileEntity) return (VoxelPrinterTileEntity) te;
            else {
                set(PRINTER_POS, new Integer[0]);
                return null;
            }
        } else return null;
    }

    public void placePrinter(BlockPos pos) {
        BlockState printerState = StarTech.Blocks.voxel_printer.defaultBlockState();
        this.entity.level.setBlock(pos, printerState, 11);
        TileEntity te = this.entity.level.getBlockEntity(pos);

        Optional<Ability> creatorAbility = this.container.getAbilities().stream().filter(ability -> ability instanceof AbilityVoxelCreator).findFirst();
        if (te instanceof VoxelPrinterTileEntity && creatorAbility.isPresent() && !creatorAbility.get().get(AbilityVoxelCreator.LOADED_MODEL).isEmpty()) {
            set(PRINTER_POS, new Integer[]{pos.getX(), pos.getY(), pos.getZ()});

            //TODO item type
            ((VoxelPrinterTileEntity) te).setBlueprint(VoxelItem.getVoxels(creatorAbility.get().get(AbilityVoxelCreator.LOADED_MODEL).getList("voxels", 10)));
        }
    }
}
