package com.nic.st.abilities.conditions;

import com.nic.st.StarTech;
import com.nic.st.abilities.AbilityVoxelPrint;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.threetag.threecore.ability.Ability;
import net.threetag.threecore.ability.condition.Condition;

public class PrinterCondition extends Condition {

    public PrinterCondition(Ability ability) {
        super(StarTech.ConditionTypes.printer_condition, ability);
    }

    @Override
    public boolean test(LivingEntity livingEntity) {
        if (!(ability instanceof AbilityVoxelPrint)) return false;

        if (((AbilityVoxelPrint) ability).getPrinter() == null) return true;

        RayTraceResult hitVec = livingEntity.pick(5.0f, 0.0f, false);

        if (!(hitVec instanceof BlockRayTraceResult)) return false;

        return livingEntity.level.getBlockEntity(((BlockRayTraceResult) hitVec).getBlockPos()) == ((AbilityVoxelPrint) ability).getPrinter();
    }
}
