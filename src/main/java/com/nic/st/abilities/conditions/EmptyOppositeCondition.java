package com.nic.st.abilities.conditions;

import com.nic.st.StarTech;
import net.minecraft.entity.LivingEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.util.Hand;
import net.threetag.threecore.ability.Ability;
import net.threetag.threecore.ability.condition.Condition;

public class EmptyOppositeCondition extends Condition {

    public EmptyOppositeCondition(Ability ability) {
        super(StarTech.ConditionTypes.empty_opposite, ability);
    }

    @Override
    public boolean test(LivingEntity livingEntity) {
        Hand hand = this.ability.container.getId().getPath().equals(EquipmentSlotType.MAINHAND.toString().toLowerCase()) ? Hand.OFF_HAND : Hand.MAIN_HAND;
        return livingEntity.getItemInHand(hand).isEmpty();
    }
}
