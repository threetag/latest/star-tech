package com.nic.st.abilities.conditions;

import com.nic.st.StarTech;
import com.nic.st.abilities.AbilityVoxelPrint;
import net.minecraft.entity.LivingEntity;
import net.threetag.threecore.ability.Ability;
import net.threetag.threecore.ability.AbilityHelper;
import net.threetag.threecore.ability.condition.Condition;
import net.threetag.threecore.util.threedata.EnumSync;
import net.threetag.threecore.util.threedata.StringThreeData;
import net.threetag.threecore.util.threedata.ThreeData;

public class HasPrinterCondition extends Condition {

    public static final ThreeData<String> ABILITY_ID = new StringThreeData("ability_id").setSyncType(EnumSync.SELF).enableSetting("ability_id", "The id for the ability that must have a printer saved for the condition to be true.");

    public HasPrinterCondition(Ability ability) {
        super(StarTech.ConditionTypes.has_printer, ability);
    }

    @Override
    public boolean test(LivingEntity livingEntity) {
        Ability a = !get(ABILITY_ID).equals("") ? AbilityHelper.getAbilityById(livingEntity, this.dataManager.get(ABILITY_ID), this.ability.container) : this.ability;
        return a instanceof AbilityVoxelPrint && ((AbilityVoxelPrint) a).getPrinter() != null;
    }

    public void registerData() {
        super.registerData();
        this.dataManager.register(ABILITY_ID, "");
    }
}
