package com.nic.st.abilities.conditions;

import com.nic.st.StarTech;
import net.minecraft.entity.LivingEntity;
import net.threetag.threecore.ability.Ability;
import net.threetag.threecore.ability.AbilityHelper;
import net.threetag.threecore.ability.condition.Condition;
import net.threetag.threecore.util.threedata.CompoundNBTThreeData;
import net.threetag.threecore.util.threedata.EnumSync;
import net.threetag.threecore.util.threedata.StringThreeData;
import net.threetag.threecore.util.threedata.ThreeData;

public class EmptyDataCondition extends Condition {

    public static final ThreeData<String> ABILITY_ID = new StringThreeData("ability_id").setSyncType(EnumSync.SELF).enableSetting("ability_id", "The id for the ability that must have storage data for the condition to be true.");
    public static final ThreeData<String> TAG_ID = new StringThreeData("tag_id").setSyncType(EnumSync.SELF).enableSetting("tag_id", "The id for the tag that must not have data for the condition to be true.");

    public EmptyDataCondition(Ability ability) {
        super(StarTech.ConditionTypes.empty_data, ability);
    }

    @Override
    public boolean test(LivingEntity livingEntity) {
        Ability a = !get(ABILITY_ID).equals("") ? AbilityHelper.getAbilityById(livingEntity, this.dataManager.get(ABILITY_ID), this.ability.container) : this.ability;
        if (a == null) return false;
        ThreeData<?> data = a.getDataManager().getDataByName(get(TAG_ID));
        return (data instanceof CompoundNBTThreeData && a.get((CompoundNBTThreeData) data).isEmpty());
    }

    public void registerData() {
        super.registerData();
        this.dataManager.register(ABILITY_ID, "");
        this.dataManager.register(TAG_ID, "");
    }
}
