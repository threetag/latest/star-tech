package com.nic.st.abilities.conditions;

import com.nic.st.StarTech;
import com.nic.st.abilities.AbilityVoxelCreator;
import net.minecraft.entity.LivingEntity;
import net.threetag.threecore.ability.Ability;
import net.threetag.threecore.ability.AbilityHelper;
import net.threetag.threecore.ability.condition.Condition;
import net.threetag.threecore.util.threedata.EnumSync;
import net.threetag.threecore.util.threedata.StringThreeData;
import net.threetag.threecore.util.threedata.ThreeData;

public class HasCreatorCondition extends Condition {

    public static final ThreeData<String> ABILITY_ID = new StringThreeData("ability_id").setSyncType(EnumSync.SELF).enableSetting("ability_id", "The id for the ability that must have storage data for the condition to be true.");

    public HasCreatorCondition(Ability ability) {
        super(StarTech.ConditionTypes.has_creator, ability);
    }

    @Override
    public boolean test(LivingEntity livingEntity) {
        Ability a = !get(ABILITY_ID).equals("") ? AbilityHelper.getAbilityById(livingEntity, this.dataManager.get(ABILITY_ID), this.ability.container) : this.ability;
        return a instanceof AbilityVoxelCreator && ((AbilityVoxelCreator) a).getCreator() != null;
    }

    public void registerData() {
        super.registerData();
        this.dataManager.register(ABILITY_ID, "");
    }
}
