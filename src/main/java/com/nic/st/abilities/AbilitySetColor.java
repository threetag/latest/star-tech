package com.nic.st.abilities;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.nic.st.StarTech;
import com.nic.st.blocks.VoxelCreatorBlock;
import com.nic.st.blocks.tileentity.VoxelCreatorTileEntity;
import com.nic.st.client.gui.SetColorScreen;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.AbstractGui;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.vector.Vector3i;
import net.threetag.threecore.ability.Ability;
import net.threetag.threecore.util.RenderUtil;
import net.threetag.threecore.util.icon.ItemIcon;

public class AbilitySetColor extends Ability {

    public static Item[] iconItems = new Item[]{Items.WHITE_DYE, Items.ORANGE_DYE, Items.MAGENTA_DYE, Items.LIGHT_BLUE_DYE, Items.YELLOW_DYE, Items.LIME_DYE, Items.PINK_DYE, Items.GRAY_DYE, Items.LIGHT_GRAY_DYE, Items.CYAN_DYE, Items.PURPLE_DYE, Items.BLUE_DYE, Items.BROWN_DYE, Items.GREEN_DYE, Items.RED_DYE, Items.BLACK_DYE};

    public AbilitySetColor() {
        super(StarTech.AbilityTypes.set_color);
    }

    @Override
    public void action(LivingEntity entity) {
        if (entity.level.isClientSide) {
            RayTraceResult hitVec = entity.pick(5.0f, 0.0f, false);
            if (!(hitVec instanceof BlockRayTraceResult)) return;
            BlockPos pos = ((BlockRayTraceResult) hitVec).getBlockPos();
            TileEntity te = entity.level.getBlockEntity(pos);
            if (!(entity.level.getBlockState(pos).getBlock() instanceof VoxelCreatorBlock) || !(te instanceof VoxelCreatorTileEntity))
                return;
            Vector3i finalVoxelPos = VoxelCreatorBlock.getVoxel(hitVec, pos);
            for (Direction.Axis value : Direction.Axis.values())
                if (finalVoxelPos.get(value) < 0 || finalVoxelPos.get(value) > 15) return;
            VoxelShape hitShape = Block.box(finalVoxelPos.getX(), finalVoxelPos.getY(), finalVoxelPos.getZ(), finalVoxelPos.getX() + 1, finalVoxelPos.getY() + 1, finalVoxelPos.getZ() + 1);
            ((VoxelCreatorTileEntity) te).getVoxels().forEach(voxelShapeData -> voxelShapeData.shape.toAabbs().forEach(voxelShapeBB -> hitShape.toAabbs().forEach(hitShapeBB -> {
                if (voxelShapeBB.contains(hitShapeBB.getCenter()) && Minecraft.getInstance().screen == null) {
                    Minecraft.getInstance().setScreen(new SetColorScreen(voxelShapeData.state, pos, voxelShapeData.color));
                }
            })));
        }
    }

    @Override
    public void drawIcon(Minecraft mc, MatrixStack stack, AbstractGui gui, int x, int y) {
        int iconIndex = (int) ((mc.level.getGameTime() % (iconItems.length * 10)) / 10);
        RenderUtil.setCurrentAbilityInIconRendering(this);
        if (this.getDataManager().has(ICON) && (!(this.getDataManager().get(ICON) instanceof ItemIcon) || ((ItemIcon) this.getDataManager().get(ICON)).stack.getItem() != Blocks.BARRIER.asItem())) {
            this.getDataManager().get(ICON).draw(mc, stack, x, y);
        } else new ItemIcon(iconItems[iconIndex]).draw(mc, stack, x, y);
    }
}
