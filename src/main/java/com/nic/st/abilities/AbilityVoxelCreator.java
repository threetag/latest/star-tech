package com.nic.st.abilities;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.nic.st.StarTech;
import com.nic.st.blocks.VoxelCreatorBlock;
import com.nic.st.blocks.tileentity.VoxelCreatorTileEntity;
import com.nic.st.items.VoxelItem;
import com.nic.st.util.VoxelShapeData;
import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.AbstractGui;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.math.vector.Vector3i;
import net.threetag.threecore.ability.Ability;
import net.threetag.threecore.util.RenderUtil;
import net.threetag.threecore.util.icon.ItemIcon;
import net.threetag.threecore.util.threedata.*;

import java.awt.*;
import java.util.Optional;

public class AbilityVoxelCreator extends Ability {

    public static final ThreeData<Integer> TYPE = new IntegerThreeData("creator_type").setSyncType(EnumSync.SELF);
    public static final ThreeData<Integer[]> CREATOR_POS = new IntegerArrayThreeData("creator").setSyncType(EnumSync.SELF);
    public static final ThreeData<CompoundNBT> LOADED_MODEL = new CompoundNBTThreeData("loaded_model").setSyncType(EnumSync.SELF);

    public AbilityVoxelCreator() {
        super(StarTech.AbilityTypes.switch_creator_type);
    }

    @Override
    public void action(LivingEntity entity) {
        switchCreatorType(entity);
    }

    @Override
    public void drawIcon(Minecraft mc, MatrixStack stack, AbstractGui gui, int x, int y) {
        RenderUtil.setCurrentAbilityInIconRendering(this);
        if (this.get(LOADED_MODEL).isEmpty()) {
            VoxelCreatorTileEntity.CreatorType.values()[this.get(TYPE)].icon.draw(mc, stack, x, y);
        } else {
            ItemStack model = new ItemStack(StarTech.Items.voxel_item);
            VoxelItem.saveVoxels(model, VoxelItem.getVoxels(get(LOADED_MODEL).getList("voxels", 10)));
            new ItemIcon(model).draw(mc, stack, x, y);
        }
    }

    private void switchCreatorType(LivingEntity entity) {
        if (!entity.level.isClientSide) {
            if (get(LOADED_MODEL).isEmpty()) {
                int creatorType = get(TYPE) + 1;
                if (creatorType >= VoxelCreatorTileEntity.CreatorType.values().length)
                    creatorType = 0;
                set(TYPE, creatorType);
            } else {
                set(LOADED_MODEL, new CompoundNBT());
            }
        }
    }

    public VoxelCreatorTileEntity getCreator() {
        if (this.entity == null) return null;
        if (get(CREATOR_POS).length > 1) {
            BlockPos pos = new BlockPos(get(CREATOR_POS)[0], get(CREATOR_POS)[1], get(CREATOR_POS)[2]);
            TileEntity te = this.entity.level.getBlockEntity(pos);
            if (te instanceof VoxelCreatorTileEntity) return (VoxelCreatorTileEntity) te;
            else {
                set(CREATOR_POS, new Integer[0]);
                return null;
            }
        } else return null;
    }

    public void placeCreator(BlockPos pos, ItemStack stack) {
        BlockState creatorState = StarTech.Blocks.voxel_creator.defaultBlockState();
        this.entity.level.setBlock(pos, creatorState, 11);
        if (this.entity instanceof ServerPlayerEntity)
            CriteriaTriggers.PLACED_BLOCK.trigger((ServerPlayerEntity) this.entity, pos, stack);
        TileEntity te = this.entity.level.getBlockEntity(pos);
        if (te instanceof VoxelCreatorTileEntity) {
            set(CREATOR_POS, new Integer[]{pos.getX(), pos.getY(), pos.getZ()});
            if (get(LOADED_MODEL).isEmpty())
                ((VoxelCreatorTileEntity) te).creatorType = VoxelCreatorTileEntity.CreatorType.values()[this.get(TYPE)];
            else {
                ((VoxelCreatorTileEntity) te).creatorType = VoxelCreatorTileEntity.CreatorType.valueOf(get(LOADED_MODEL).getString("type"));
                ((VoxelCreatorTileEntity) te).getVoxels().addAll(VoxelItem.getVoxels(get(LOADED_MODEL).getList("voxels", 10)));
            }
            te.setChanged();
            te.getLevel().sendBlockUpdated(pos, creatorState, creatorState, 3);
        }
    }

    public void placeVoxel(Vector3d clickedPos, Direction direction, AbilitySwitchMaterial materialAbility) {
        Vector3d pos = clickedPos;
        switch (direction) {
            case DOWN:
                pos = new Vector3d(pos.x, pos.y - 0.0625, pos.z);
                break;
            case UP:
                pos = new Vector3d(pos.x, pos.y + 0.0624, pos.z);
                break;
            case NORTH:
                pos = new Vector3d(pos.x, pos.y, pos.z - 0.0625);
                break;
            case SOUTH:
                pos = new Vector3d(pos.x, pos.y, pos.z + 0.0624);
                break;
            case WEST:
                pos = new Vector3d(pos.x - 0.0625, pos.y, pos.z);
                break;
            case EAST:
                pos = new Vector3d(pos.x + 0.0624, pos.y, pos.z);
                break;
        }
        VoxelCreatorTileEntity te = getCreator();
        if (te == null || !te.getBlockPos().equals(new BlockPos(pos))) return;

        BlockState shapeState = AbilitySwitchMaterial.POSSIBLE_MATERIALS[materialAbility.get(AbilitySwitchMaterial.MATERIAL)];
        Vector3d voxelPos = pos.subtract(te.getBlockPos().getX(), te.getBlockPos().getY(), te.getBlockPos().getZ()).multiply(16, 16, 16);
        Vector3i finalVoxelPos = new Vector3i(Math.floor(voxelPos.x), Math.floor(voxelPos.y), Math.floor(voxelPos.z));
        for (Direction.Axis value : Direction.Axis.values())
            if (finalVoxelPos.get(value) < 0 || finalVoxelPos.get(value) > 15)
                return;
        Optional<VoxelShapeData> existingShape = te.getVoxels().stream().filter(voxelShapeData -> voxelShapeData.state == shapeState).findFirst();
        if (existingShape.isPresent()) existingShape.get().addVoxel(finalVoxelPos);
        else
            te.getVoxels().add(new VoxelShapeData(Block.box(finalVoxelPos.getX(), finalVoxelPos.getY(), finalVoxelPos.getZ(), finalVoxelPos.getX() + 1, finalVoxelPos.getY() + 1, finalVoxelPos.getZ() + 1), shapeState, Color.WHITE));
        te.setChanged();
        this.entity.level.sendBlockUpdated(te.getBlockPos(), te.getBlockState(), te.getBlockState(), 3);
    }

    public void removeVoxel(BlockRayTraceResult hitVec) {
        VoxelCreatorTileEntity te = getCreator();

        if (te == null || !te.getBlockPos().equals(new BlockPos(hitVec.getBlockPos()))) return;
        Vector3i finalVoxelPos = VoxelCreatorBlock.getVoxel(hitVec, hitVec.getBlockPos());
        for (Direction.Axis value : Direction.Axis.values())
            if (finalVoxelPos.get(value) < 0 || finalVoxelPos.get(value) > 15) return;
        te.getVoxels().forEach(voxelShapeData -> {
            if (voxelShapeData.state != null) voxelShapeData.removeVoxel(finalVoxelPos);
        });
        te.setChanged();
        te.getLevel().sendBlockUpdated(te.getBlockPos(), te.getBlockState(), te.getBlockState(), 3);
    }

    public void registerData() {
        super.registerData();
        this.dataManager.register(TYPE, 0);
        this.dataManager.register(CREATOR_POS, new Integer[0]);
        this.dataManager.register(LOADED_MODEL, new CompoundNBT());
    }
}
