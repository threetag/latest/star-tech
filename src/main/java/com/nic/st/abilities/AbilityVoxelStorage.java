package com.nic.st.abilities;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.nic.st.StarTech;
import com.nic.st.blocks.tileentity.VoxelCreatorTileEntity;
import com.nic.st.client.gui.SaveLoadScreen;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.AbstractGui;
import net.minecraft.entity.LivingEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.threetag.threecore.ability.Ability;
import net.threetag.threecore.util.RenderUtil;
import net.threetag.threecore.util.icon.TexturedIcon;
import net.threetag.threecore.util.threedata.CompoundNBTThreeData;
import net.threetag.threecore.util.threedata.EnumSync;
import net.threetag.threecore.util.threedata.ThreeData;

import java.util.Optional;

public class AbilityVoxelStorage extends Ability {

    public static final ThreeData<CompoundNBT> STORAGE = new CompoundNBTThreeData("storage").setSyncType(EnumSync.SELF);
    public static final TexturedIcon LOAD_ICON = new TexturedIcon(new ResourceLocation(StarTech.MODID, "textures/gui/icons.png"), 16, 0, 16, 16, 256, 256);
    public static final TexturedIcon SAVE_ICON = new TexturedIcon(new ResourceLocation(StarTech.MODID, "textures/gui/icons.png"), 32, 0, 16, 16, 256, 256);

    public AbilityVoxelStorage() {
        super(StarTech.AbilityTypes.storage);
    }

    @Override
    public void action(LivingEntity entity) {
        Optional<Ability> creator = getContainer().getAbilities().stream().filter(ability -> ability instanceof AbilityVoxelCreator).findFirst();
        if (creator.isPresent()) {
            VoxelCreatorTileEntity te = ((AbilityVoxelCreator) creator.get()).getCreator();
            if (Minecraft.getInstance().screen == null) {
                if (te != null && entity.level.isClientSide) {
                    RayTraceResult hitVec = entity.pick(5.0f, 0.0f, false);
                    if (!(hitVec instanceof BlockRayTraceResult) || entity.level.getBlockEntity(((BlockRayTraceResult) hitVec).getBlockPos()) != te)
                        return;

                    Minecraft.getInstance().setScreen(new SaveLoadScreen(getVoxelModels(), this, ((BlockRayTraceResult) hitVec).getBlockPos()));
                } else if (te == null) {
                    Minecraft.getInstance().setScreen(new SaveLoadScreen(getVoxelModels(), this, null));
                }
            }
        }
    }

    @Override
    public void drawIcon(Minecraft mc, MatrixStack stack, AbstractGui gui, int x, int y) {
        RenderUtil.setCurrentAbilityInIconRendering(this);
        if (getContainer() != null && getContainer().getAbilities() != null) {
            Optional<Ability> creator = getContainer().getAbilities().stream().filter(ability -> ability instanceof AbilityVoxelCreator).findFirst();
            if (creator.isPresent()) {
                VoxelCreatorTileEntity te = ((AbilityVoxelCreator) creator.get()).getCreator();
                if (te != null) {
                    SAVE_ICON.draw(mc, stack, x, y);
                } else {
                    LOAD_ICON.draw(mc, stack, x, y);
                }
            }
        }
    }

    public void saveVoxelModel(CompoundNBT nbt, int index) {
        CompoundNBT storageNBT = get(STORAGE).copy();
        storageNBT.put("model" + index, nbt);
        set(STORAGE, storageNBT);
    }

    public CompoundNBT[] getVoxelModels() {
        CompoundNBT storageNBT = get(STORAGE);
        CompoundNBT[] models = new CompoundNBT[8];
        for (int i = 0; i < models.length; i++)
            models[i] = storageNBT.getCompound("model" + i);
        return models;
    }

    public void registerData() {
        super.registerData();
        this.dataManager.register(STORAGE, new CompoundNBT());
    }
}
