package com.nic.st.abilities;

import com.nic.st.StarTech;
import com.nic.st.items.NanobotWelderItem;
import net.minecraft.entity.LivingEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.threetag.threecore.ability.Ability;
import net.threetag.threecore.util.icon.TexturedIcon;

public class AbilityEjectMemoryCard extends Ability {

    public AbilityEjectMemoryCard() {
        super(StarTech.AbilityTypes.eject_memory_card);
    }

    @Override
    public void action(LivingEntity entity) {
        Hand hand = this.container.getId().getPath().equals(EquipmentSlotType.MAINHAND.toString().toLowerCase()) ? Hand.OFF_HAND : Hand.MAIN_HAND;
        Hand otherHand = hand == Hand.MAIN_HAND ? Hand.OFF_HAND : Hand.MAIN_HAND;
        if (entity.getItemInHand(hand).isEmpty()) {
            AbilityVoxelStorage welderStorage = NanobotWelderItem.getStorageAbility(entity, entity.getItemInHand(otherHand));
            if (welderStorage != null) {
                ItemStack stack = new ItemStack(StarTech.Items.memory_card);
                entity.setItemInHand(hand, stack);
                stack.setTag(welderStorage.get(AbilityVoxelStorage.STORAGE).copy());
                welderStorage.set(AbilityVoxelStorage.STORAGE, new CompoundNBT());
            }
        }
    }

    @Override
    public void registerData() {
        super.registerData();
        set(ICON, new TexturedIcon(new ResourceLocation(StarTech.MODID, "textures/gui/icons.png"), 48, 0, 16, 16, 256, 256));
    }
}
