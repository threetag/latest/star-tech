package com.nic.st.client.model;

import com.nic.st.StarTech;
import com.nic.st.blocks.tileentity.AbstractVoxelTileEntity;
import com.nic.st.util.GGUtils;
import com.nic.st.util.VoxelEntityModelUtils;
import com.nic.st.util.VoxelShapeData;
import net.minecraft.block.BlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.model.BakedQuad;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemOverrideList;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockDisplayReader;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.model.data.IModelData;
import net.minecraftforge.client.model.data.ModelDataMap;
import net.minecraftforge.client.model.data.ModelProperty;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import javax.annotation.Nullable;
import java.util.*;

@Mod.EventBusSubscriber(modid = StarTech.MODID, value = Dist.CLIENT)
public class VoxelModel implements IBakedModel {

    public static final ModelProperty<Optional<AbstractVoxelTileEntity>> VOXEL_TILE = new ModelProperty<>();

    public static final ModelCache NBT_CACHE = new NBTVoxelModel.NBTCache();
    public static final VoxelCreatorModel.VoxelTileCache TILE_CACHE = new VoxelCreatorModel.VoxelTileCache();

    private final IBakedModel baseModel;
    private final VoxelModelOverriderList overrider = new VoxelModelOverriderList();

    public VoxelModel(IBakedModel model) {
        this.baseModel = model;
    }

    @SubscribeEvent
    public static void onClientTick(TickEvent.ClientTickEvent event) {
        if (event.phase == TickEvent.Phase.END) {
            if (Minecraft.getInstance().level == null) {
                TILE_CACHE.flush();
                NBT_CACHE.flush();
            } else if (Minecraft.getInstance().level.getGameTime() % 20 == 0) {
                TILE_CACHE.update(Minecraft.getInstance().level);
                NBT_CACHE.update(Minecraft.getInstance().level);
            }
        }
    }

    @Override
    public List<BakedQuad> getQuads(@Nullable BlockState p_200117_1_, @Nullable Direction p_200117_2_, Random p_200117_3_) {
        return new ArrayList<>();
    }

    @Override
    public List<BakedQuad> getQuads(@Nullable BlockState state, @Nullable Direction side, Random rand, IModelData extraData) {
        return getCustomVoxelModel(extraData).getQuads(state, side, rand);
    }

    @Override
    public IModelData getModelData(IBlockDisplayReader world, BlockPos pos, BlockState state, IModelData tileData) {
        ModelDataMap.Builder builder = new ModelDataMap.Builder();
        builder.withInitial(VOXEL_TILE, Optional.empty());
        ModelDataMap map = builder.build();
        TileEntity te = world.getBlockEntity(pos);
        if (te instanceof AbstractVoxelTileEntity)
            map.setData(VOXEL_TILE, Optional.of(((AbstractVoxelTileEntity) te)));
        return map;
    }

    private IBakedModel getCustomVoxelModel(IModelData data) {
        IBakedModel model = baseModel;
        if (!data.hasProperty(VOXEL_TILE))
            return model;
        Optional<AbstractVoxelTileEntity> creator = data.getData(VOXEL_TILE);
        if (!creator.isPresent()) return model;
        model = new VoxelCreatorModel(baseModel, creator.get());
        return model;
    }

    protected List<BakedQuad> getVoxelShapeQuads(VoxelShapeData... voxelShapeData) {
        ArrayList<BakedQuad> q = new ArrayList<>();
        Arrays.stream(voxelShapeData).forEach(voxelShapeData1 -> {
            TextureAtlasSprite[] sprites;
            if (voxelShapeData1.state != null)
                sprites = new TextureAtlasSprite[]{Minecraft.getInstance().getBlockRenderer().getBlockModel(voxelShapeData1.state).getParticleIcon()};
            else
                sprites = VoxelEntityModelUtils.TextureHandler.getTextures(VoxelEntityModelUtils.ModelPart.valueOf(voxelShapeData1.modelPart));
            voxelShapeData1.shape.toAabbs().forEach(axisAlignedBB -> q.addAll(GGUtils.createdBakedQuadsForBox(axisAlignedBB, voxelShapeData1.color, sprites)));
        });
        return q;
    }

    @Override
    public ItemOverrideList getOverrides() {
        return overrider;
    }

    @Override
    public boolean useAmbientOcclusion() {
        return baseModel.useAmbientOcclusion();
    }

    @Override
    public boolean isGui3d() {
        return baseModel.isGui3d();
    }

    @Override
    public boolean usesBlockLight() {
        return baseModel.usesBlockLight();
    }

    @Override
    public boolean isCustomRenderer() {
        return baseModel.isCustomRenderer();
    }

    @Override
    public TextureAtlasSprite getParticleIcon() {
        return baseModel.getParticleIcon();
    }

    public static class VoxelModelOverriderList extends ItemOverrideList {
        @Nullable
        @Override
        public IBakedModel resolve(IBakedModel baseModel, ItemStack stackIn, @Nullable ClientWorld worldIn, @Nullable LivingEntity entityIn) {
            return new NBTVoxelModel(baseModel, stackIn);
        }
    }

    public static abstract class ModelCache {
        abstract List<BakedQuad> getQuads(IBakedModel model, @Nullable BlockState state, @Nullable Direction direction, Random r);

        abstract void flush();

        abstract void update(World world);
    }
}
