package com.nic.st.client.model;

import com.nic.st.StarTech;
import net.minecraft.client.renderer.BlockModelShapes;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.ModelBakeEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = StarTech.MODID, value = Dist.CLIENT, bus = Mod.EventBusSubscriber.Bus.MOD)
public class ModelReplacer {

    @SubscribeEvent
    public static void onModelBakeEvent(ModelBakeEvent event) {
        ModelResourceLocation itemModelResourceLocation = new ModelResourceLocation(new ResourceLocation(StarTech.MODID, "voxel_item"), "inventory");
        IBakedModel baseModel = event.getModelRegistry().get(itemModelResourceLocation);
        if (baseModel != null && !(baseModel instanceof VoxelModel)) {
            VoxelModel voxelModel = new VoxelModel(baseModel);
            event.getModelRegistry().put(itemModelResourceLocation, voxelModel);
        }

        StarTech.Blocks.voxel_creator.getStateDefinition().getPossibleStates().forEach(state -> {
            ModelResourceLocation modelLoc = BlockModelShapes.stateToModelLocation(state);
            IBakedModel baseModel1 = event.getModelRegistry().get(modelLoc);
            VoxelModel model = new VoxelModel(baseModel1);
            event.getModelRegistry().put(modelLoc, model);
        });

        StarTech.Blocks.voxel_printer.getStateDefinition().getPossibleStates().forEach(state -> {
            ModelResourceLocation modelLoc = BlockModelShapes.stateToModelLocation(state);
            IBakedModel baseModel1 = event.getModelRegistry().get(modelLoc);
            VoxelModel model = new VoxelModel(baseModel1);
            event.getModelRegistry().put(modelLoc, model);
        });
    }
}
