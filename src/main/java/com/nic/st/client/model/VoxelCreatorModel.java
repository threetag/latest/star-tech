package com.nic.st.client.model;

import com.nic.st.blocks.tileentity.AbstractVoxelTileEntity;
import com.nic.st.util.VoxelShapeData;
import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.model.BakedQuad;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.util.Direction;
import net.minecraft.world.World;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class VoxelCreatorModel extends VoxelModel {

    public AbstractVoxelTileEntity creator;

    public VoxelCreatorModel(IBakedModel model, AbstractVoxelTileEntity creator) {
        super(model);
        this.creator = creator;
    }

    @Override
    public List<BakedQuad> getQuads(@Nullable BlockState state, @Nullable Direction direction, Random r) {
        return TILE_CACHE.getQuads(this, state, direction, r);
    }

    public static class VoxelTileCache extends ModelCache {

        private static final HashMap<AbstractVoxelTileEntity, List<BakedQuad>> CREATOR_CACHE = new HashMap<>();

        @Override
        List<BakedQuad> getQuads(IBakedModel model, @Nullable BlockState state, @Nullable Direction direction, Random r) {
            if (model instanceof VoxelCreatorModel && ((VoxelCreatorModel) model).creator != null && direction == null) { //TODO direction culling
                if (CREATOR_CACHE.containsKey(((VoxelCreatorModel) model).creator))
                    return CREATOR_CACHE.get(((VoxelCreatorModel) model).creator);
                VoxelShapeData[] voxelData = ((VoxelCreatorModel) model).creator.getModifiedVoxels().toArray(new VoxelShapeData[0]);
                List<BakedQuad> quads = ((VoxelCreatorModel) model).getVoxelShapeQuads(voxelData);
                CREATOR_CACHE.put(((VoxelCreatorModel) model).creator, quads);
                return quads;
            }
            return new ArrayList<>();
        }

        @Override
        void flush() {
            if (!CREATOR_CACHE.isEmpty())
                CREATOR_CACHE.clear();
        }

        public void flush(AbstractVoxelTileEntity tileEntity) {
            CREATOR_CACHE.remove(tileEntity);
        }

        @Override
        void update(World world) {
            HashMap<AbstractVoxelTileEntity, List<BakedQuad>> updatedCreatorCache = new HashMap<>();

            CREATOR_CACHE.forEach((tileEntity, bakedQuads) -> {
                if (world.blockEntityList.contains(tileEntity))
                    updatedCreatorCache.put(tileEntity, bakedQuads);
            });

            flush();
            CREATOR_CACHE.putAll(updatedCreatorCache);
        }
    }
}
