package com.nic.st.client.model;

import com.nic.st.items.VoxelItem;
import com.nic.st.util.VoxelShapeData;
import net.minecraft.block.BlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.model.BakedQuad;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.ListNBT;
import net.minecraft.util.Direction;
import net.minecraft.world.World;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class NBTVoxelModel extends VoxelModel {

    private final ListNBT nbt;

    public NBTVoxelModel(IBakedModel model, ListNBT nbt) {
        super(model);
        this.nbt = nbt;
    }

    public NBTVoxelModel(IBakedModel model, ItemStack stack) {
        super(model);
        this.nbt = VoxelItem.getVoxelNBTList(stack);
    }

    @Override
    public List<BakedQuad> getQuads(@Nullable BlockState state, @Nullable Direction direction, Random r) {
        return NBT_CACHE.getQuads(this, state, direction, r);
    }

    public static class NBTCache extends ModelCache {

        private static final HashMap<ListNBT, CompoundCacheHolder> NBT_CACHE = new HashMap<>();

        private static final int CACHED_ITEM_TIME = 6000;

        @Override
        List<BakedQuad> getQuads(IBakedModel model, @Nullable BlockState state, @Nullable Direction direction, Random r) {
            if (model instanceof NBTVoxelModel && ((NBTVoxelModel) model).nbt != null && direction == null) {
                if (NBT_CACHE.containsKey(((NBTVoxelModel) model).nbt))
                    return NBT_CACHE.get(((NBTVoxelModel) model).nbt).getQuads(Minecraft.getInstance().level);
                List<BakedQuad> quads = ((NBTVoxelModel) model).getVoxelShapeQuads(VoxelItem.getVoxels(((NBTVoxelModel) model).nbt).toArray(new VoxelShapeData[0]));
                NBT_CACHE.put(((NBTVoxelModel) model).nbt, new CompoundCacheHolder(quads, Minecraft.getInstance().level));
                return quads;
            }
            return new ArrayList<>();
        }

        @Override
        void flush() {
            if (!NBT_CACHE.isEmpty())
                NBT_CACHE.clear();
        }

        @Override
        void update(World world) {
            HashMap<ListNBT, CompoundCacheHolder> updatedListCache = new HashMap<>();

            NBT_CACHE.forEach((stack, compoundCacheHolder) -> {
                if (world.getGameTime() <= compoundCacheHolder.lastAccess + CACHED_ITEM_TIME)
                    updatedListCache.put(stack, compoundCacheHolder);
            });
            flush();
            NBT_CACHE.putAll(updatedListCache);
        }
    }

    public static class CompoundCacheHolder {
        long lastAccess;
        List<BakedQuad> quads;

        public CompoundCacheHolder(List<BakedQuad> quads, World world) {
            this.quads = quads;
            this.lastAccess = world.getGameTime();
        }

        public List<BakedQuad> getQuads(World world) {
            this.lastAccess = world.getGameTime();
            return this.quads;
        }
    }
}
