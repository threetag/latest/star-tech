package com.nic.st.client;

import com.nic.st.StarTech;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.*;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.particles.BlockParticleData;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.event.ParticleFactoryRegisterEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import javax.annotation.Nullable;

public class WelderParticle extends DeceleratingParticle {

    WelderParticle(ClientWorld world, double x, double y, double z, double xSpeed, double ySpeed, double zSpeed, BlockState state) {
        super(world, x, y, z, xSpeed, ySpeed, zSpeed);
        this.setSprite(Minecraft.getInstance().getBlockRenderer().getBlockModelShaper().getParticleIcon(state));
        float c = (float) Math.random() * 0.5f + 0.5f;
        this.setColor(c, c, c);
        this.scale((float) Math.random() * 0.25f);
    }

    public IParticleRenderType getRenderType() {
        return IParticleRenderType.TERRAIN_SHEET;
    }

    public float getQuadSize(float p_217561_1_) {
        return this.quadSize * MathHelper.clamp(((float) this.age + p_217561_1_) / (float) this.lifetime * 32.0F, 0.0F, 1.0F);
    }

    public void tick() {
        this.xo = this.x;
        this.yo = this.y;
        this.zo = this.z;

        if (this.age++ >= this.lifetime) {
            this.remove();
        } else {
            this.move(this.xd, this.yd, this.zd);
            this.xd *= 0.96F;
            this.yd *= 0.96F;
            this.zd *= 0.96F;
            this.oRoll = this.roll;
            this.roll += (float) Math.PI * 2.0F;
            if (this.onGround) {
                this.xd *= 0.7F;
                this.zd *= 0.7F;
                this.oRoll = this.roll = 0.0F;
            }

        }
    }

    @OnlyIn(Dist.CLIENT)
    @Mod.EventBusSubscriber(value = Dist.CLIENT, bus = Mod.EventBusSubscriber.Bus.MOD)
    public static class Factory implements IParticleFactory<BlockParticleData> {

        public Factory(IAnimatedSprite p_i51109_1_) {
        }

        @SubscribeEvent
        public static void registerFactory(ParticleFactoryRegisterEvent event) {
            Minecraft.getInstance().particleEngine.register(StarTech.ParticleTypes.welder, WelderParticle.Factory::new);
        }

        @Nullable
        public Particle createParticle(BlockParticleData data, ClientWorld world, double x, double y, double z, double xSpeed, double ySpeed, double zSpeed) {
            BlockState blockstate = data.getState();
            if (!blockstate.isAir() && blockstate.getRenderShape() == BlockRenderType.INVISIBLE) {
                return null;
            } else {
                return new WelderParticle(world, x, y, z, xSpeed, ySpeed, zSpeed, blockstate);
            }
        }
    }
}