package com.nic.st.client.gui;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import com.nic.st.StarTech;
import com.nic.st.network.SetColorMessage;
import net.minecraft.block.BlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.chat.NarratorChatListener;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.AbstractSlider;
import net.minecraft.client.gui.widget.button.AbstractButton;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldVertexBufferUploader;
import net.minecraft.client.renderer.texture.AtlasTexture;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Matrix4f;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;

import java.awt.*;

public class SetColorScreen extends Screen {

    public Color color;
    public BlockState state;
    public BlockPos pos;

    public SetColorScreen(BlockState state, BlockPos pos, Color color) {
        super(NarratorChatListener.NO_TITLE);
        this.state = state;
        this.pos = pos;
        this.color = color;
    }

    @Override
    protected void init() {
        this.addButton(new AbstractButton(this.width / 2 - 40, this.height - 110, 80, 20, new StringTextComponent("Set Color")) {
            @Override
            public void onPress() {
                StarTech.simpleNetworkWrapper.sendToServer(new SetColorMessage(pos, state.getBlock(), color));
                Minecraft.getInstance().setScreen(null);
            }
        });
        this.addButton(new AbstractSlider(this.width / 2 - 40, this.height - 90, 80, 20, new StringTextComponent("Red: 255").withStyle(TextFormatting.RED), ((float) color.getRed() / 255.0f)) {
            @Override
            protected void updateMessage() {
                this.setMessage(new StringTextComponent("Red: " + (int) (this.value * 255.0)).withStyle(TextFormatting.RED));
            }

            @Override
            protected void applyValue() {
                color = new Color((int) (this.value * 255.0), color.getGreen(), color.getBlue());
            }
        });

        this.addButton(new AbstractSlider(this.width / 2 - 40, this.height - 70, 80, 20, new StringTextComponent("Green: 255").withStyle(TextFormatting.GREEN), ((float) color.getGreen() / 255.0f)) {
            @Override
            protected void updateMessage() {
                this.setMessage(new StringTextComponent("Green: " + (int) (this.value * 255.0)).withStyle(TextFormatting.GREEN));
            }

            @Override
            protected void applyValue() {
                color = new Color(color.getRed(), (int) (this.value * 255.0), color.getBlue());
            }
        });

        this.addButton(new AbstractSlider(this.width / 2 - 40, this.height - 50, 80, 20, new StringTextComponent("Blue: 255").withStyle(TextFormatting.BLUE), ((float) color.getBlue() / 255.0f)) {
            @Override
            protected void updateMessage() {
                this.setMessage(new StringTextComponent("Blue: " + (int) (this.value * 255.0)).withStyle(TextFormatting.BLUE));
            }

            @Override
            protected void applyValue() {
                color = new Color(color.getRed(), color.getGreen(), (int) (this.value * 255.0));
            }
        });
    }

    public void render(MatrixStack stack, int mouseX, int mouseY, float partialTicks) {
        this.renderBackground(stack);
        this.getMinecraft().getTextureManager().bind(AtlasTexture.LOCATION_BLOCKS);
        blit(stack, this.width / 2 - 25, this.height - 165, 0, 50, 50, Minecraft.getInstance().getBlockRenderer().getBlockModel(state).getParticleIcon(), color);
        super.render(stack, mouseX, mouseY, partialTicks);
    }

    public static void blit(MatrixStack stack, int x, int y, int z, int width, int height, TextureAtlasSprite sprite, Color color) {
        innerBlit(stack.last().pose(), x, x + width, y, y + height, z, sprite.getU0(), sprite.getU1(), sprite.getV0(), sprite.getV1(), color);
    }

    private static void innerBlit(Matrix4f stack, int x0, int x1, int y1, int y0, int z, float minU, float maxU, float minV, float maxV, Color color) {
        BufferBuilder bufferbuilder = Tessellator.getInstance().getBuilder();
        bufferbuilder.begin(7, DefaultVertexFormats.POSITION_COLOR_TEX);
        bufferbuilder.vertex(stack, (float) x0, (float) y0, (float) z).color(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha()).uv(minU, maxV).endVertex();
        bufferbuilder.vertex(stack, (float) x1, (float) y0, (float) z).color(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha()).uv(maxU, maxV).endVertex();
        bufferbuilder.vertex(stack, (float) x1, (float) y1, (float) z).color(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha()).uv(maxU, minV).endVertex();
        bufferbuilder.vertex(stack, (float) x0, (float) y1, (float) z).color(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha()).uv(minU, minV).endVertex();
        bufferbuilder.end();
        RenderSystem.enableAlphaTest();
        WorldVertexBufferUploader.end(bufferbuilder);
    }

    @Override
    public boolean isPauseScreen() {
        return false;
    }
}
