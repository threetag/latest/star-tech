package com.nic.st.client.gui;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import com.nic.st.StarTech;
import com.nic.st.abilities.AbilityVoxelStorage;
import com.nic.st.items.VoxelItem;
import com.nic.st.network.LoadVoxelMessage;
import com.nic.st.network.SaveVoxelMessage;
import net.minecraft.block.Blocks;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.chat.NarratorChatListener;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.button.AbstractButton;
import net.minecraft.client.renderer.*;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.texture.AtlasTexture;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Matrix4f;
import net.minecraft.util.text.StringTextComponent;

import java.awt.*;

public class SaveLoadScreen extends Screen {

    private final CompoundNBT[] models;
    private final String buttonString;
    private final AbilityVoxelStorage ability;
    private final BlockPos pos;

    public SaveLoadScreen(CompoundNBT[] models, AbilityVoxelStorage ability, BlockPos pos) {
        super(NarratorChatListener.NO_TITLE);
        this.models = models;
        this.ability = ability;
        this.pos = pos;
        buttonString = pos == null ? "LOAD" : "SAVE";
    }

    public static void blit(MatrixStack stack, int x, int y, int z, int width, int height, TextureAtlasSprite sprite, Color color) {
        innerBlit(stack.last().pose(), x, x + width, y, y + height, z, sprite.getU0(), sprite.getU1(), sprite.getV0(), sprite.getV1(), color);
    }

    private static void innerBlit(Matrix4f stack, int x0, int x1, int y1, int y0, int z, float minU, float maxU, float minV, float maxV, Color color) {
        BufferBuilder bufferbuilder = Tessellator.getInstance().getBuilder();
        bufferbuilder.begin(7, DefaultVertexFormats.POSITION_COLOR_TEX);
        bufferbuilder.vertex(stack, (float) x0, (float) y0, (float) z).color(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha()).uv(minU, maxV).endVertex();
        bufferbuilder.vertex(stack, (float) x1, (float) y0, (float) z).color(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha()).uv(maxU, maxV).endVertex();
        bufferbuilder.vertex(stack, (float) x1, (float) y1, (float) z).color(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha()).uv(maxU, minV).endVertex();
        bufferbuilder.vertex(stack, (float) x0, (float) y1, (float) z).color(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha()).uv(minU, minV).endVertex();
        bufferbuilder.end();
        RenderSystem.enableAlphaTest();
        WorldVertexBufferUploader.end(bufferbuilder);
    }

    @Override
    protected void init() {
        for (int x = 0; x < 4; x++) {
            for (int y = 0; y < 2; y++) {
                int i = (y * 4) + x;
                if (pos != null || !models[i].isEmpty())
                    this.addButton(new AbstractButton(this.width / 2 - 220 + (120 * x), this.height / 2 - 40 + (120 * y), 80, 20, new StringTextComponent(buttonString)) {
                        @Override
                        public void onPress() {
                            if (pos != null)
                                StarTech.simpleNetworkWrapper.sendToServer(new SaveVoxelMessage(Minecraft.getInstance().player.getId(), ability.container.getId(), ability.getId(), pos, i));
                            else
                                StarTech.simpleNetworkWrapper.sendToServer(new LoadVoxelMessage(Minecraft.getInstance().player.getId(), ability.container.getId(), ability.getId(), i));
                            Minecraft.getInstance().setScreen(null);
                        }
                    });
            }
        }
    }

    public void render(MatrixStack stack, int mouseX, int mouseY, float partialTicks) {
        this.renderBackground(stack);
        this.getMinecraft().getTextureManager().bind(AtlasTexture.LOCATION_BLOCKS);
//        blit(stack, this.width / 2 - 25, this.height - 165, 0, 50, 50, Minecraft.getInstance().getBlockRenderer().getBlockModel(state).getParticleIcon(), color);
        for (int x = 0; x < 4; x++) {
            for (int y = 0; y < 2; y++) {
                blit(stack, this.width / 2 - 220 + (120 * x), this.height / 2 - 130 + (120 * y), 0, 80, 90, Minecraft.getInstance().getBlockRenderer().getBlockModel(Blocks.IRON_BLOCK.defaultBlockState()).getParticleIcon(), Color.WHITE);
            }
        }
        super.render(stack, mouseX, mouseY, partialTicks);
        for (int x = 0; x < 4; x++) {
            for (int y = 0; y < 2; y++) {
                int i = (y * 4) + x;
                if (this.models[i].isEmpty())
                    this.font.draw(stack, "Empty", this.width / 2 - 193 + (120 * x), this.height / 2 - 90 + (120 * y), 0);
                else {
                    ItemStack itemStack = new ItemStack(StarTech.Items.voxel_item);
                    VoxelItem.saveVoxels(itemStack, VoxelItem.getVoxels(this.models[i].getList("voxels", 10)));
                    renderGuiItem(itemStack, this.width / 2 - 188 + (120 * x), this.height / 2 - 95 + (120 * y), getMinecraft().getItemRenderer().getModel(itemStack, null, null));
                }
            }
        }

    }

    private void renderGuiItem(ItemStack p_191962_1_, int p_191962_2_, int p_191962_3_, IBakedModel p_191962_4_) {
        RenderSystem.pushMatrix();
        this.getMinecraft().getTextureManager().bind(AtlasTexture.LOCATION_BLOCKS);
        this.getMinecraft().getTextureManager().getTexture(AtlasTexture.LOCATION_BLOCKS).setFilter(false, false);
        RenderSystem.enableRescaleNormal();
        RenderSystem.enableAlphaTest();
        RenderSystem.defaultAlphaFunc();
        RenderSystem.enableBlend();
        RenderSystem.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
        RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        RenderSystem.translatef((float) p_191962_2_, (float) p_191962_3_, 100.0F + this.itemRenderer.blitOffset);
        RenderSystem.translatef(8.0F, 8.0F, 0.0F);
        RenderSystem.scalef(1.0F, -1.0F, 1.0F);
        RenderSystem.scalef(16.0F, 16.0F, 16.0F);
        MatrixStack matrixstack = new MatrixStack();
        matrixstack.scale(4.9f, 4.9f, 4.9f);
        IRenderTypeBuffer.Impl irendertypebuffer$impl = Minecraft.getInstance().renderBuffers().bufferSource();
        boolean flag = !p_191962_4_.usesBlockLight();
        if (flag) {
            RenderHelper.setupForFlatItems();
        }

        itemRenderer.render(p_191962_1_, ItemCameraTransforms.TransformType.GUI, false, matrixstack, irendertypebuffer$impl, 15728880, OverlayTexture.NO_OVERLAY, p_191962_4_);
        irendertypebuffer$impl.endBatch();
        RenderSystem.enableDepthTest();
        if (flag) {
            RenderHelper.setupFor3DItems();
        }

        RenderSystem.disableAlphaTest();
        RenderSystem.disableRescaleNormal();
        RenderSystem.popMatrix();
    }

    @Override
    public boolean isPauseScreen() {
        return false;
    }
}
