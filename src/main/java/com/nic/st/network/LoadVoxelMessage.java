package com.nic.st.network;

import com.nic.st.abilities.AbilityVoxelCreator;
import com.nic.st.abilities.AbilityVoxelStorage;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkEvent;
import net.threetag.threecore.ability.Ability;
import net.threetag.threecore.ability.AbilityHelper;
import net.threetag.threecore.ability.container.IAbilityContainer;

import java.util.Optional;
import java.util.function.Supplier;

public class LoadVoxelMessage {
    public int index;
    public int entityID;
    public ResourceLocation containerId;
    public String abilityId;

    public LoadVoxelMessage(int entityID, ResourceLocation containerId, String abilityId, int index) {
        this.index = index;
        this.entityID = entityID;
        this.containerId = containerId;
        this.abilityId = abilityId;
    }

    public LoadVoxelMessage(PacketBuffer buffer) {
        index = buffer.readInt();
        entityID = buffer.readInt();
        containerId = new ResourceLocation(buffer.readUtf(32767));
        abilityId = buffer.readUtf(32767);
    }

    public void toBytes(PacketBuffer buffer) {
        buffer.writeInt(index);
        buffer.writeInt(entityID);
        buffer.writeUtf(containerId.toString());
        buffer.writeUtf(abilityId);
    }

    public void handle(Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> {
            PlayerEntity player = ctx.get().getSender();
            IAbilityContainer container = AbilityHelper.getAbilityContainerFromId(player, this.containerId);
            if (container != null) {
                Ability storageAbility = container.getAbilityMap().get(this.abilityId);
                Optional<Ability> creatorAbility = container.getAbilities().stream().filter(ability -> ability instanceof AbilityVoxelCreator).findFirst();
                if (storageAbility instanceof AbilityVoxelStorage && creatorAbility.isPresent()) {
                    creatorAbility.get().set(AbilityVoxelCreator.LOADED_MODEL, ((AbilityVoxelStorage) storageAbility).getVoxelModels()[index].copy());
                }
            }
        });
        ctx.get().setPacketHandled(true);
    }

}
