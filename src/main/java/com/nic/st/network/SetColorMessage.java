package com.nic.st.network;

import com.nic.st.blocks.tileentity.VoxelCreatorTileEntity;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.NetworkEvent;
import net.minecraftforge.registries.ForgeRegistries;

import java.awt.*;
import java.util.function.Supplier;

public class SetColorMessage {

    public BlockPos pos;
    public Block block;
    public Color color;

    public SetColorMessage(BlockPos pos, Block block, Color color) {
        this.pos = pos;
        this.block = block;
        this.color = color;
    }

    public SetColorMessage(PacketBuffer buffer) {
        pos = buffer.readBlockPos();
        block = ForgeRegistries.BLOCKS.getValue(buffer.readResourceLocation());
        color = new Color(buffer.readInt());
    }

    public void toBytes(PacketBuffer buffer) {
        buffer.writeBlockPos(pos);
        buffer.writeResourceLocation(block.getRegistryName());
        buffer.writeInt(color.getRGB());
    }

    public void handle(Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> {
            PlayerEntity player = ctx.get().getSender();
            TileEntity te = player.level.getBlockEntity(pos);
            BlockState state = player.level.getBlockState(pos);
            if (te instanceof VoxelCreatorTileEntity) {
                ((VoxelCreatorTileEntity) te).getVoxels().forEach(voxelShapeData -> {
                    if (voxelShapeData.state.getBlock().equals(block))
                        voxelShapeData.color = color;
                });
                te.setChanged();
                player.level.sendBlockUpdated(pos, state, state, 3);
            }
        });
        ctx.get().setPacketHandled(true);
    }

}
