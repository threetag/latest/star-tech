package com.nic.st.network;

import com.nic.st.abilities.AbilityVoxelStorage;
import com.nic.st.blocks.tileentity.VoxelCreatorTileEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.NetworkEvent;
import net.threetag.threecore.ability.Ability;
import net.threetag.threecore.ability.AbilityHelper;
import net.threetag.threecore.ability.container.IAbilityContainer;

import java.util.function.Supplier;

public class SaveVoxelMessage {

    public BlockPos pos;
    public int index;
    public int entityID;
    public ResourceLocation containerId;
    public String abilityId;

    public SaveVoxelMessage(int entityID, ResourceLocation containerId, String abilityId, BlockPos pos, int index) {
        this.pos = pos;
        this.index = index;
        this.entityID = entityID;
        this.containerId = containerId;
        this.abilityId = abilityId;
    }

    public SaveVoxelMessage(PacketBuffer buffer) {
        pos = buffer.readBlockPos();
        index = buffer.readInt();
        entityID = buffer.readInt();
        containerId = new ResourceLocation(buffer.readUtf(32767));
        abilityId = buffer.readUtf(32767);
    }

    public void toBytes(PacketBuffer buffer) {
        buffer.writeBlockPos(pos);
        buffer.writeInt(index);
        buffer.writeInt(entityID);
        buffer.writeUtf(containerId.toString());
        buffer.writeUtf(abilityId);
    }

    public void handle(Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> {
            PlayerEntity player = ctx.get().getSender();
            TileEntity te = player.level.getBlockEntity(pos);
            if (te instanceof VoxelCreatorTileEntity) {
                IAbilityContainer container = AbilityHelper.getAbilityContainerFromId(player, this.containerId);
                if (container != null) {
                    Ability ability = container.getAbilityMap().get(this.abilityId);
                    if (ability instanceof AbilityVoxelStorage) {
                        CompoundNBT model = new CompoundNBT();
                        ListNBT voxelTags = new ListNBT();
                        ((VoxelCreatorTileEntity) te).getVoxels().forEach(voxelShapeData -> voxelTags.add(voxelShapeData.serializeNBT()));
                        model.put("voxels", voxelTags);
                        model.putString("type", ((VoxelCreatorTileEntity) te).creatorType.name());
                        ((AbilityVoxelStorage) ability).saveVoxelModel(model, index);
                    }
                }
            }
        });
        ctx.get().setPacketHandled(true);
    }

}
