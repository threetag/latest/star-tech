package com.nic.st.blocks;

import com.nic.st.blocks.tileentity.VoxelCreatorTileEntity;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.math.vector.Vector3i;


public class VoxelCreatorBlock extends AbstractVoxelBlock {

    public static final Material HOLOGRAM = new Material.Builder(MaterialColor.COLOR_LIGHT_BLUE).noCollider().build();

    public VoxelCreatorBlock() {
        super(AbstractBlock.Properties.of(HOLOGRAM).strength(-1.0F, 3600000.0F).noDrops().noOcclusion().dynamicShape(), (blockState, iBlockReader) -> new VoxelCreatorTileEntity());
    }

    public static Vector3i getVoxel(RayTraceResult hitVec, BlockPos pos) {
        Vector3d breakPos = hitVec.getLocation();
        if (hitVec instanceof BlockRayTraceResult)
            switch (((BlockRayTraceResult) hitVec).getDirection()) {
                case UP:
                    breakPos = breakPos.subtract(0, 0.01, 0);
                    break;
                case SOUTH:
                    breakPos = breakPos.subtract(0, 0, 0.01);
                    break;
                case EAST:
                    breakPos = breakPos.subtract(0.01, 0, 0);
                    break;
            }

        breakPos = breakPos.subtract(pos.getX(), pos.getY(), pos.getZ()).multiply(16, 16, 16);
        return new Vector3i(Math.floor(breakPos.x), Math.floor(breakPos.y), Math.floor(breakPos.z));
    }
}
