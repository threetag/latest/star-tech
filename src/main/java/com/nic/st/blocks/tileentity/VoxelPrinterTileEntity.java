package com.nic.st.blocks.tileentity;

import com.nic.st.StarTech;
import com.nic.st.items.VoxelItem;
import com.nic.st.util.VoxelShapeData;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.util.math.vector.Vector3d;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class VoxelPrinterTileEntity extends AbstractVoxelTileEntity implements ITickableTileEntity {

    private final List<VoxelShapeData> finished_voxels = new ArrayList<>();
    private VoxelShapeData holo_voxels = new VoxelShapeData(VoxelShapes.empty(), StarTech.Blocks.hologram.defaultBlockState(), Color.WHITE);
    private int progress = 0;

    public VoxelPrinterTileEntity() {
        super(StarTech.TileEntityTypes.voxel_printer);
    }

    public static Vector3d getVoxel(BlockRayTraceResult hitVec, BlockPos pos) {
        Vector3d breakPos = hitVec.getLocation();
        switch (hitVec.getDirection()) {
            case UP:
                breakPos = breakPos.subtract(0, 0.01, 0);
                break;
            case SOUTH:
                breakPos = breakPos.subtract(0, 0, 0.01);
                break;
            case EAST:
                breakPos = breakPos.subtract(0.01, 0, 0);
                break;
        }

        breakPos = breakPos.subtract(pos.getX(), pos.getY(), pos.getZ()).multiply(16, 16, 16);
        return new Vector3d(Math.floor(breakPos.x), Math.floor(breakPos.y), Math.floor(breakPos.z)).multiply(0.0625, 0.0625, 0.0625).add(0.03125, 0.03125, 0.03125);
    }

    @Override
    public void load(BlockState p_230337_1_, CompoundNBT compound) {
        super.load(p_230337_1_, compound);

        holo_voxels = new VoxelShapeData(compound.getCompound("holo_voxels"));

        finished_voxels.clear();
        ListNBT voxelTags = compound.getList("finished_voxels", 10);
        voxelTags.forEach(inbt -> {
            if (inbt instanceof CompoundNBT)
                finished_voxels.add(new VoxelShapeData((CompoundNBT) inbt));
        });

        progress = compound.getInt("progress");
    }

    @Override
    public CompoundNBT save(CompoundNBT compound) {
        CompoundNBT nbt = super.save(compound);

        nbt.put("holo_voxels", holo_voxels.serializeNBT());

        ListNBT finishedTags = new ListNBT();
        this.finished_voxels.forEach(voxelShapeData -> finishedTags.add(voxelShapeData.serializeNBT()));
        nbt.put("finished_voxels", finishedTags);

        nbt.putInt("progress", progress);

        return nbt;
    }

    @Override
    public void tick() {
        if (progress < 16 * 3) {
            progress++;
            this.setChanged();
            this.level.sendBlockUpdated(getBlockPos(), getBlockState(), getBlockState(), 3);
        }
    }

    @Override
    public List<VoxelShapeData> getModifiedVoxels() {
        List<VoxelShapeData> modifiedVoxels = new ArrayList<>(super.getModifiedVoxels());

        if (progress < 16 * 3) {
            VoxelShape holoPanel = Block.box(0, progress / 3.0, 0, 16, progress / 3.0 + 1, 16);
            VoxelShape cutout = Block.box(0, progress / 3.0, 0, 16, 16, 16);


            modifiedVoxels = modifiedVoxels.stream().map(voxelShapeData -> {
                VoxelShape s = VoxelShapes.join(voxelShapeData.shape, cutout, IBooleanFunction.ONLY_FIRST);
                return (voxelShapeData.state == null) ? new VoxelShapeData(s, voxelShapeData.modelPart, voxelShapeData.color)
                        : new VoxelShapeData(s, voxelShapeData.state, voxelShapeData.color);
            }).collect(Collectors.toList());
            modifiedVoxels.add(new VoxelShapeData(holoPanel, StarTech.Blocks.hologram.defaultBlockState(), Color.WHITE));
        }

        return modifiedVoxels;
    }

    public void setBlueprint(List<VoxelShapeData> voxelsIn) {
        final VoxelShape[] holoShape = {VoxelShapes.empty()};
        voxelsIn.forEach(voxelShapeData -> holoShape[0] = VoxelShapes.or(voxelShapeData.shape, holoShape[0]).optimize());
        this.holo_voxels = new VoxelShapeData(holoShape[0], StarTech.Blocks.hologram.defaultBlockState(), Color.WHITE);
        this.finished_voxels.addAll(voxelsIn);
        this.getVoxels().add(holo_voxels);

        this.setChanged();
        this.level.sendBlockUpdated(getBlockPos(), getBlockState(), getBlockState(), 3);
    }

    public boolean print(BlockRayTraceResult result, double printArea) {
        if (progress < 16 * 3) return false;
        BlockPos pos = result.getBlockPos();
        Vector3d v = getVoxel(result, pos);
        AxisAlignedBB box = new AxisAlignedBB(v.x, v.y, v.z, v.x, v.y, v.z).inflate(printArea);
        this.holo_voxels.shape = VoxelShapes.join(this.holo_voxels.shape, VoxelShapes.create(box), IBooleanFunction.ONLY_FIRST);

        if (this.holo_voxels.shape.isEmpty()) {
            if (!this.level.isClientSide) {
                ItemStack printedStack = new ItemStack(StarTech.Items.voxel_item);
                VoxelItem.saveVoxels(printedStack, this.finished_voxels);

                ItemEntity printedItemEntity = new ItemEntity(this.level, this.getBlockPos().getX(), this.getBlockPos().getY(), this.getBlockPos().getZ(), printedStack);
                printedItemEntity.setDefaultPickUpDelay();

                this.level.addFreshEntity(printedItemEntity);

                this.level.removeBlock(this.getBlockPos(), false);
            }
            return false;
        } else {
            List<VoxelShapeData> oldVoxels = getVoxels();
            getVoxels().clear();

            this.finished_voxels.forEach(vData -> {
                Optional<VoxelShapeData> oldShape = oldVoxels.stream().filter(oldData -> vData.state == oldData.state).findFirst();
                VoxelShape newShape = VoxelShapes.join(vData.shape, VoxelPrinterTileEntity.this.holo_voxels.shape, IBooleanFunction.ONLY_FIRST);
                if (oldShape.isPresent())
                    newShape = VoxelShapes.or(newShape, oldShape.get().shape);
                VoxelPrinterTileEntity.this.getVoxels().add(new VoxelShapeData(newShape, vData.state, vData.color));
            });


            getVoxels().add(this.holo_voxels);
            this.setChanged();
            this.level.sendBlockUpdated(getBlockPos(), getBlockState(), getBlockState(), 3);
            return true;
        }
    }
}
