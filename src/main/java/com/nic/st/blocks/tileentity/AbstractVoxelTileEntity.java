package com.nic.st.blocks.tileentity;

import com.nic.st.client.model.VoxelModel;
import com.nic.st.util.VoxelShapeData;
import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractVoxelTileEntity extends TileEntity {

    private final List<VoxelShapeData> voxels = new ArrayList<>();

    public AbstractVoxelTileEntity(TileEntityType<?> p_i48289_1_) {
        super(p_i48289_1_);
    }

    public List<VoxelShapeData> getVoxels() {
        return this.voxels;
    }

    public List<VoxelShapeData> getModifiedVoxels() {
        return this.voxels;
    }

    @Override
    public void load(BlockState p_230337_1_, CompoundNBT compound) {
        super.load(p_230337_1_, compound);
        voxels.clear();
        ListNBT voxelTags = compound.getList("voxels", 10);
        voxelTags.forEach(inbt -> {
            if (inbt instanceof CompoundNBT)
                voxels.add(new VoxelShapeData((CompoundNBT) inbt));
        });
    }

    @Override
    public CompoundNBT save(CompoundNBT compound) {
        CompoundNBT nbt = super.save(compound);
        ListNBT voxelTags = new ListNBT();
        this.voxels.forEach(voxelShapeData -> voxelTags.add(voxelShapeData.serializeNBT()));
        nbt.put("voxels", voxelTags);
        return nbt;
    }

    @Override
    public CompoundNBT getUpdateTag() {
        return save(super.getUpdateTag());
    }

    @Override
    public void handleUpdateTag(BlockState state, CompoundNBT tag) {
        load(state, tag);
        VoxelModel.TILE_CACHE.flush(this);
    }

    @Nullable
    public SUpdateTileEntityPacket getUpdatePacket() {
        return new SUpdateTileEntityPacket(this.worldPosition, 0, this.getUpdateTag());
    }

    @Override
    public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
        assert this.level != null;
        this.handleUpdateTag(this.level.getBlockState(this.getBlockPos()), pkt.getTag());
        this.level.sendBlockUpdated(this.getBlockPos(), this.getBlockState(), this.getBlockState(), 3);
    }
}
