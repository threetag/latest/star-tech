package com.nic.st.blocks.tileentity;

import com.nic.st.StarTech;
import com.nic.st.abilities.AbilityVoxelCreator;
import com.nic.st.util.VoxelEntityModelUtils;
import com.nic.st.util.VoxelShapeData;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.threetag.threecore.ability.Ability;
import net.threetag.threecore.ability.AbilityHelper;
import net.threetag.threecore.util.icon.IIcon;
import net.threetag.threecore.util.icon.TexturedIcon;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class VoxelCreatorTileEntity extends AbstractVoxelTileEntity implements ITickableTileEntity {

    public CreatorType creatorType = CreatorType.GUN;
    private int progress = 0;

    public VoxelCreatorTileEntity() {
        super(StarTech.TileEntityTypes.voxel_creator);
    }

    @Override
    public void tick() {
        if (this.level.getGameTime() % 20 == 0) {
            AxisAlignedBB searchBox = new AxisAlignedBB(this.getBlockPos().getX() - 10, this.getBlockPos().getY() - 10, this.getBlockPos().getZ() - 10, this.getBlockPos().getX() + 10, this.getBlockPos().getY() + 10, this.getBlockPos().getZ() + 10);
            Predicate<? super Ability> abilityPredicate = (Predicate<Ability>) ability -> ability instanceof AbilityVoxelCreator && ((AbilityVoxelCreator) ability).getCreator() == VoxelCreatorTileEntity.this;
            Predicate<? super Entity> predicate = (Predicate<Entity>) entity -> entity instanceof LivingEntity && AbilityHelper.getAbilities((LivingEntity) entity).stream().anyMatch(abilityPredicate);
            List<Entity> entityList = this.level.getEntities((Entity) null, searchBox, predicate);
            if (entityList.isEmpty()) {
                this.level.removeBlock(this.getBlockPos(), true);
            }
        }
        if (progress < 16 * 3) {
            progress++;
            this.setChanged();
            this.level.sendBlockUpdated(getBlockPos(), getBlockState(), getBlockState(), 3);
        }
    }

    @Override
    public List<VoxelShapeData> getModifiedVoxels() {
        List<VoxelShapeData> modifiedVoxels = new ArrayList<>(super.getModifiedVoxels());
        if (this.creatorType.modelPart != null)
            VoxelEntityModelUtils.addModelPart(this.creatorType.modelPart, modifiedVoxels);

        if (progress < 16 * 3) {
            VoxelShape holoPanel = Block.box(0, progress / 3.0, 0, 16, progress / 3.0 + 1, 16);
            VoxelShape cutout = Block.box(0, progress / 3.0, 0, 16, 16, 16);


            modifiedVoxels = modifiedVoxels.stream().map(voxelShapeData -> {
                VoxelShape s = VoxelShapes.join(voxelShapeData.shape, cutout, IBooleanFunction.ONLY_FIRST);
                return (voxelShapeData.state == null) ? new VoxelShapeData(s, voxelShapeData.modelPart, voxelShapeData.color)
                        : new VoxelShapeData(s, voxelShapeData.state, voxelShapeData.color);
            }).collect(Collectors.toList());
            modifiedVoxels.add(new VoxelShapeData(holoPanel, StarTech.Blocks.hologram.defaultBlockState(), Color.WHITE));
        }

        return modifiedVoxels;
    }

    @Override
    public void load(BlockState p_230337_1_, CompoundNBT compound) {
        super.load(p_230337_1_, compound);
        this.creatorType = CreatorType.valueOf(compound.getString("type"));
        this.progress = compound.getInt("progress");
    }

    @Override
    public CompoundNBT save(CompoundNBT compound) {
        CompoundNBT nbt = super.save(compound);
        nbt.putString("type", this.creatorType.name());
        nbt.putInt("progress", this.progress);
        return nbt;
    }

    public enum CreatorType {
        GUN(VoxelEntityModelUtils.ModelPart.HORIZONTAL_ARM, new TexturedIcon(new ResourceLocation(StarTech.MODID, "textures/gui/icons.png"), 0, 0, 16, 16, 256, 256));

        public VoxelEntityModelUtils.ModelPart modelPart;
        public IIcon icon;

        CreatorType(VoxelEntityModelUtils.ModelPart modelPart, IIcon icon) {
            this.modelPart = modelPart;
            this.icon = icon;
        }
    }
}