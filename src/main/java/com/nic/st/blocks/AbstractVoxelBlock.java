package com.nic.st.blocks;

import com.nic.st.blocks.tileentity.AbstractVoxelTileEntity;
import com.nic.st.client.model.VoxelModel;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import javax.annotation.Nullable;
import java.util.function.BiFunction;

public abstract class AbstractVoxelBlock extends Block {

    protected BiFunction<BlockState, IBlockReader, AbstractVoxelTileEntity> tileEntityCreator;

    public AbstractVoxelBlock(Properties properties, BiFunction<BlockState, IBlockReader, AbstractVoxelTileEntity> tileEntityCreator) {
        super(properties);
        this.tileEntityCreator = tileEntityCreator;
    }

    @Override
    public void onRemove(BlockState state, World world, BlockPos pos, BlockState newState, boolean p_196243_5_) {
        if (world.isClientSide && world.getBlockEntity(pos) instanceof AbstractVoxelTileEntity) {
            VoxelModel.TILE_CACHE.flush((AbstractVoxelTileEntity) world.getBlockEntity(pos));
        }
        super.onRemove(state, world, pos, newState, p_196243_5_);
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader world, BlockPos pos, ISelectionContext ctx) {
        TileEntity te = world.getBlockEntity(pos);
        if (te instanceof AbstractVoxelTileEntity)
            return VoxelShapes.or(VoxelShapes.empty(), ((AbstractVoxelTileEntity) te).getModifiedVoxels().stream().map(voxelShapeData -> voxelShapeData.shape).toArray(VoxelShape[]::new)).optimize();
        return super.getShape(state, world, pos, ctx);
    }

    @Override
    public VoxelShape getInteractionShape(BlockState state, IBlockReader world, BlockPos pos) {
        TileEntity te = world.getBlockEntity(pos);
        if (te instanceof AbstractVoxelTileEntity)
            return VoxelShapes.or(VoxelShapes.empty(), ((AbstractVoxelTileEntity) te).getModifiedVoxels().stream().map(voxelShapeData -> voxelShapeData.shape).toArray(VoxelShape[]::new)).optimize();
        return super.getInteractionShape(state, world, pos);
    }

    @Override
    public VoxelShape getCollisionShape(BlockState p_220071_1_, IBlockReader p_220071_2_, BlockPos p_220071_3_, ISelectionContext p_220071_4_) {
        return VoxelShapes.empty();
    }

    @Override
    public boolean hasTileEntity(BlockState state) {
        return true;
    }

    @Nullable
    @Override
    public TileEntity createTileEntity(BlockState state, IBlockReader world) {
        return tileEntityCreator.apply(state, world);
    }

    public VoxelShape getVisualShape(BlockState p_230322_1_, IBlockReader p_230322_2_, BlockPos p_230322_3_, ISelectionContext p_230322_4_) {
        return VoxelShapes.empty();
    }

    @OnlyIn(Dist.CLIENT)
    public float getShadeBrightness(BlockState p_220080_1_, IBlockReader p_220080_2_, BlockPos p_220080_3_) {
        return 1.0F;
    }

    public boolean propagatesSkylightDown(BlockState p_200123_1_, IBlockReader p_200123_2_, BlockPos p_200123_3_) {
        return true;
    }
}