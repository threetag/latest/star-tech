package com.nic.st.blocks;

import com.nic.st.blocks.tileentity.VoxelPrinterTileEntity;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;

public class VoxelPrinterBlock extends AbstractVoxelBlock {

    public static final Material HOLOGRAM = new Material.Builder(MaterialColor.COLOR_LIGHT_BLUE).noCollider().build();

    public VoxelPrinterBlock() {
        super(AbstractBlock.Properties.of(HOLOGRAM).strength(-1.0F, 3600000.0F).noDrops().noOcclusion().dynamicShape(), (blockState, iBlockReader) -> new VoxelPrinterTileEntity());
    }

}