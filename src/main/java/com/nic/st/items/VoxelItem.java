package com.nic.st.items;

import com.nic.st.util.VoxelShapeData;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;

import java.util.ArrayList;
import java.util.List;

public class VoxelItem extends Item {
    public VoxelItem() {
        super(new Properties().stacksTo(1));
    }

    public static List<VoxelShapeData> getVoxels(ItemStack stack) {
        return getVoxels(getVoxelNBTList(stack));
    }

    public static List<VoxelShapeData> getVoxels(ListNBT listNBT) {
        List<VoxelShapeData> voxels = new ArrayList<>();
        listNBT.forEach(inbt -> {
            if (inbt instanceof CompoundNBT)
                voxels.add(new VoxelShapeData((CompoundNBT) inbt));
        });
        return voxels;
    }

    public static ListNBT getVoxelNBTList(ItemStack stack) {
        return stack.getOrCreateTag().getList("voxels", 10);
    }

    public static void saveVoxels(ItemStack stack, List<VoxelShapeData> voxels) {
        CompoundNBT nbt = stack.getOrCreateTag();
        ListNBT voxelTags = new ListNBT();
        voxels.forEach(voxelShapeData -> voxelTags.add(voxelShapeData.serializeNBT()));
        nbt.put("voxels", voxelTags);
    }
}
