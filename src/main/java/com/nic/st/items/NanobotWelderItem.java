package com.nic.st.items;

import com.nic.st.StarTech;
import com.nic.st.abilities.*;
import com.nic.st.abilities.conditions.*;
import com.nic.st.blocks.VoxelCreatorBlock;
import net.minecraft.block.BlockState;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.threetag.threecore.ability.Ability;
import net.threetag.threecore.ability.AbilityGenerator;
import net.threetag.threecore.ability.AbilityHelper;
import net.threetag.threecore.ability.condition.ActionCondition;
import net.threetag.threecore.ability.condition.Condition;
import net.threetag.threecore.ability.condition.HeldCondition;
import net.threetag.threecore.ability.container.IAbilityContainer;
import net.threetag.threecore.capability.ItemAbilityContainer;
import net.threetag.threecore.item.AbilityItem;

import java.util.Optional;

public class NanobotWelderItem extends AbilityItem {

    public NanobotWelderItem() {
        super(new Item.Properties().stacksTo(1));
        this.addAbility(new AbilityGenerator("creator", () -> {
            AbilityVoxelCreator a = new AbilityVoxelCreator();
            a.getConditionManager().addCondition(new ActionCondition(a));

            Condition c = new HasCreatorCondition(a);
            c.set(HasCreatorCondition.ABILITY_ID, "creator");
            c.set(Condition.INVERT, true);
            a.getConditionManager().addCondition(c);

            c = new HasPrinterCondition(a);
            c.set(HasPrinterCondition.ABILITY_ID, "print");
            c.set(Condition.INVERT, true);
            a.getConditionManager().addCondition(c);
            return a;
        }));
        this.addAbility(new AbilityGenerator("switch_material", () -> {
            AbilitySwitchMaterial a = new AbilitySwitchMaterial();
            a.getConditionManager().addCondition(new ActionCondition(a));

            Condition c = new HasCreatorCondition(a);
            c.set(HasCreatorCondition.ABILITY_ID, "creator");
            a.getConditionManager().addCondition(c);
            return a;
        }));
        this.addAbility(new AbilityGenerator("set_color", () -> {
            AbilitySetColor a = new AbilitySetColor();
            a.getConditionManager().addCondition(new ActionCondition(a));

            Condition c = new HasCreatorCondition(a);
            c.set(HasCreatorCondition.ABILITY_ID, "creator");
            a.getConditionManager().addCondition(c);
            return a;
        }));
        this.addAbility(new AbilityGenerator("storage", () -> {
            AbilityVoxelStorage a = new AbilityVoxelStorage();
            a.getConditionManager().addCondition(new ActionCondition(a));
            Condition c = new EmptyDataCondition(a);
            c.set(Condition.INVERT, true);
            c.set(EmptyDataCondition.TAG_ID, "storage");
            a.getConditionManager().addCondition(c);
            return a;
        }));
        this.addAbility(new AbilityGenerator("eject_card", () -> {
            AbilityEjectMemoryCard a = new AbilityEjectMemoryCard();
            a.getConditionManager().addCondition(new ActionCondition(a));

            Condition c = new EmptyDataCondition(a);
            c.set(Condition.INVERT, true);
            c.set(EmptyDataCondition.TAG_ID, "storage");
            c.set(EmptyDataCondition.ABILITY_ID, "storage");
            a.getConditionManager().addCondition(c);

            c = new EmptyDataCondition(a);
            c.set(EmptyDataCondition.TAG_ID, "loaded_model");
            c.set(EmptyDataCondition.ABILITY_ID, "creator");
            a.getConditionManager().addCondition(c);

            c = new HasCreatorCondition(a);
            c.set(HasCreatorCondition.ABILITY_ID, "creator");
            c.set(Condition.INVERT, true);
            a.getConditionManager().addCondition(c);

            a.getConditionManager().addCondition(new EmptyOppositeCondition(a));
            return a;
        }));
        this.addAbility(new AbilityGenerator("print", () -> {
            AbilityVoxelPrint a = new AbilityVoxelPrint();
            a.getConditionManager().addCondition(new HeldCondition(a));

            Condition c = new PrinterCondition(a);
            c.set(Condition.ENABLING, true);
            a.getConditionManager().addCondition(c);

            c = new HasCreatorCondition(a);
            c.set(HasCreatorCondition.ABILITY_ID, "creator");
            c.set(Condition.INVERT, true);
            a.getConditionManager().addCondition(c);

            c = new EmptyDataCondition(a);
            c.set(EmptyDataCondition.TAG_ID, "loaded_model");
            c.set(EmptyDataCondition.ABILITY_ID, "creator");
            c.set(Condition.INVERT, true);
            a.getConditionManager().addCondition(c);
            return a;
        }));
    }

    @Override
    public ActionResult<ItemStack> use(World world, PlayerEntity player, Hand hand) {
        Hand otherHand = hand == Hand.MAIN_HAND ? Hand.OFF_HAND : Hand.MAIN_HAND;
        if (player.getItemInHand(otherHand).getItem() == StarTech.Items.memory_card) {
            AbilityVoxelStorage welderStorage = getStorageAbility(player, player.getItemInHand(hand));
            if (welderStorage != null) {
                CompoundNBT nbt = player.getItemInHand(otherHand).getOrCreateTag();
                nbt.putBoolean("memory_card", true);
                welderStorage.set(AbilityVoxelStorage.STORAGE, nbt.copy());
                player.setItemInHand(otherHand, ItemStack.EMPTY);
            }
        }
        return super.use(world, player, hand);
    }

    public ActionResultType useOn(ItemUseContext ctx) {
        BlockPos pos = ctx.getClickedPos().relative(ctx.getClickedFace());
        AbilityVoxelCreator creator = getCreatorAbility(ctx.getPlayer());
        AbilitySwitchMaterial material = getMaterialAbility(ctx.getPlayer());
        if (creator == null || material == null) return ActionResultType.FAIL;
        if (ctx.getLevel().isEmptyBlock(pos) && creator.getCreator() == null)
            creator.placeCreator(pos, ctx.getItemInHand());
        else
            creator.placeVoxel(ctx.getClickLocation(), ctx.getClickedFace(), material);
        return ActionResultType.FAIL;
    }

    private AbilityVoxelCreator getCreatorAbility(LivingEntity entity) {
        Optional<IAbilityContainer> container = AbilityHelper.getAbilityContainers(entity).stream().filter(iAbilityContainer -> iAbilityContainer instanceof ItemAbilityContainer && ((ItemAbilityContainer) iAbilityContainer).stack.equals(entity.getMainHandItem())).findFirst();
        if (container.isPresent()) {
            Ability creator = container.get().getAbility("creator");
            if (creator instanceof AbilityVoxelCreator) return (AbilityVoxelCreator) creator;
        }
        return null;
    }

    private AbilitySwitchMaterial getMaterialAbility(LivingEntity entity) {
        Optional<IAbilityContainer> container = AbilityHelper.getAbilityContainers(entity).stream().filter(iAbilityContainer -> iAbilityContainer instanceof ItemAbilityContainer && ((ItemAbilityContainer) iAbilityContainer).stack.equals(entity.getMainHandItem())).findFirst();
        if (container.isPresent()) {
            Ability creator = container.get().getAbility("switch_material");
            if (creator instanceof AbilitySwitchMaterial) return (AbilitySwitchMaterial) creator;
        }
        return null;
    }

    public static AbilityVoxelStorage getStorageAbility(LivingEntity entity, ItemStack stack) {
        Optional<IAbilityContainer> container = AbilityHelper.getAbilityContainers(entity).stream().filter(iAbilityContainer -> iAbilityContainer instanceof ItemAbilityContainer && ((ItemAbilityContainer) iAbilityContainer).stack.equals(stack)).findFirst();
        if (container.isPresent()) {
            Ability storage = container.get().getAbility("storage");
            if (storage instanceof AbilityVoxelStorage) return (AbilityVoxelStorage) storage;
        }
        return null;
    }

    @Override
    public boolean canAttackBlock(BlockState state, World world, BlockPos pos, PlayerEntity player) {
        RayTraceResult hitVec = player.pick(5.0f, 0.0f, false);
        if (!(hitVec instanceof BlockRayTraceResult) || !(state.getBlock() instanceof VoxelCreatorBlock) || world.isClientSide)
            return super.canAttackBlock(state, world, pos, player);
        AbilityVoxelCreator creator = getCreatorAbility(player);
        if (creator != null)
            creator.removeVoxel((BlockRayTraceResult) hitVec);
        return false;
    }
}
