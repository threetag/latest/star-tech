package com.nic.st.util;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.DoubleNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.nbt.NBTUtil;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.util.math.vector.Vector3i;
import net.minecraftforge.common.util.INBTSerializable;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class VoxelShapeData implements INBTSerializable<CompoundNBT> {

    public Color color;
    public VoxelShape shape;
    public BlockState state;
    public String modelPart;

    public VoxelShapeData(VoxelShape shape, BlockState state, Color color) {
        this.shape = shape;
        this.state = state;
        this.color = color;
    }

    public VoxelShapeData(VoxelShape shape, String modelPart, Color color) {
        this.shape = shape;
        this.color = color;
        this.modelPart = modelPart;
    }

    public VoxelShapeData(CompoundNBT nbt) {
        deserializeNBT(nbt);
    }

    public void addVoxel(Vector3i voxelPos) {
        shape = VoxelShapes.join(shape, Block.box(voxelPos.getX(), voxelPos.getY(), voxelPos.getZ(), voxelPos.getX() + 1, voxelPos.getY() + 1, voxelPos.getZ() + 1), IBooleanFunction.OR);
    }

    public void removeVoxel(Vector3i voxelPos) {
        shape = VoxelShapes.join(shape, Block.box(voxelPos.getX(), voxelPos.getY(), voxelPos.getZ(), voxelPos.getX() + 1, voxelPos.getY() + 1, voxelPos.getZ() + 1), IBooleanFunction.ONLY_FIRST);
    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT nbt = new CompoundNBT();
        ListNBT boxes = new ListNBT();
        shape.toAabbs().forEach(axisAlignedBB -> {
            boxes.add(DoubleNBT.valueOf(axisAlignedBB.minX));
            boxes.add(DoubleNBT.valueOf(axisAlignedBB.minY));
            boxes.add(DoubleNBT.valueOf(axisAlignedBB.minZ));
            boxes.add(DoubleNBT.valueOf(axisAlignedBB.maxX));
            boxes.add(DoubleNBT.valueOf(axisAlignedBB.maxY));
            boxes.add(DoubleNBT.valueOf(axisAlignedBB.maxZ));
        });
        nbt.put("voxelBoxes", boxes);
        nbt.putInt("color", color.getRGB());
        if (state != null)
            nbt.put("blockstate", NBTUtil.writeBlockState(state));
        if (modelPart != null)
            nbt.putString("modelPart", modelPart);
        return nbt;
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt) {
        ListNBT boxes = nbt.getList("voxelBoxes", 6);
        List<AxisAlignedBB> boxesBB = new ArrayList<>();
        for (int i = 0; i + 5 < boxes.size(); i += 6) {
            boxesBB.add(new AxisAlignedBB(boxes.getDouble(i), boxes.getDouble(i + 1), boxes.getDouble(i + 2), boxes.getDouble(i + 3), boxes.getDouble(i + 4), boxes.getDouble(i + 5)));
        }
        shape = VoxelShapes.empty();
        boxesBB.forEach(axisAlignedBB -> shape = VoxelShapes.or(shape, VoxelShapes.create(axisAlignedBB)).optimize());

        color = new Color(nbt.getInt("color"));

        if (nbt.contains("blockstate"))
            state = NBTUtil.readBlockState(nbt.getCompound("blockstate"));
        if (nbt.contains("modelPart"))
            modelPart = nbt.getString("modelPart");
    }
}
