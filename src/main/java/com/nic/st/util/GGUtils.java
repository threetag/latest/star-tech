package com.nic.st.util;

import com.google.common.primitives.Ints;
import net.minecraft.client.renderer.LightTexture;
import net.minecraft.client.renderer.model.BakedQuad;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.util.Direction;
import net.minecraft.util.math.AxisAlignedBB;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Methods shamelessly stolen or adapted from TheGreyGhost
 * Check him out at https://github.com/TheGreyGhost/MinecraftByExample
 */
public class GGUtils {

    public static List<BakedQuad> createdBakedQuadsForBox(AxisAlignedBB box, Color color, TextureAtlasSprite... sprites) {
        Color flippedColor = new Color(color.getBlue(), color.getGreen(), color.getRed()); //For some reason the color's red and blue get swapped?
        List<BakedQuad> quads = new ArrayList<>();
        Arrays.stream(Direction.values()).forEach(direction -> {
            float x1, x2, x3, x4 = x1 = x2 = x3 = 0;
            float y1, y2, y3, y4 = y1 = y2 = y3 = 0;
            float z1, z2, z3, z4 = z1 = z2 = z3 = 0;

            int packednormal;

            int minU = 0;
            int maxU = 16;
            int minV = 0;
            int maxV = 16;

            switch (direction) {
                case UP:
                    x1 = x2 = (float) box.maxX;
                    x3 = x4 = (float) box.minX;
                    z1 = z4 = (float) box.maxZ;
                    z2 = z3 = (float) box.minZ;
                    y1 = y2 = y3 = y4 = (float) box.maxY;

                    minU = (int) (box.minX * 16);
                    maxU = (int) (box.maxX * 16);
                    minV = (int) (box.minZ * 16);
                    maxV = (int) (box.maxZ * 16);
                    break;
                case DOWN:

                    x1 = (float) box.maxX;
                    z1 = (float) box.minZ;

                    x2 = (float) box.maxX;
                    z2 = (float) box.maxZ;

                    x3 = (float) box.minX;
                    z3 = (float) box.maxZ;

                    x4 = (float) box.minX;
                    z4 = (float) box.minZ;

                    y1 = y2 = y3 = y4 = (float) box.minY;

                    minU = (int) (box.minX * 16);
                    maxU = (int) (box.maxX * 16);
                    minV = 16 - (int) (box.maxZ * 16);
                    maxV = 16 - (int) (box.minZ * 16);
                    break;
                case NORTH:
                    x1 = x2 = (float) box.minX;
                    x3 = x4 = (float) box.maxX;
                    y1 = y4 = (float) box.minY;
                    y2 = y3 = (float) box.maxY;
                    z1 = z2 = z3 = z4 = (float) box.minZ;

                    minU = 16 - (int) (box.maxX * 16);
                    maxU = 16 - (int) (box.minX * 16);
                    minV = 16 - (int) (box.maxY * 16);
                    maxV = 16 - (int) (box.minY * 16);
                    break;
                case SOUTH:
                    x1 = x2 = (float) box.maxX;
                    x3 = x4 = (float) box.minX;
                    y1 = y4 = (float) box.minY;
                    y2 = y3 = (float) box.maxY;
                    z1 = z2 = z3 = z4 = (float) box.maxZ;

                    minU = (int) (box.minX * 16);
                    maxU = (int) (box.maxX * 16);
                    minV = 16 - (int) (box.maxY * 16);
                    maxV = 16 - (int) (box.minY * 16);
                    break;
                case WEST:
                    z1 = z2 = (float) box.maxZ;
                    z3 = z4 = (float) box.minZ;
                    y1 = y4 = (float) box.minY;
                    y2 = y3 = (float) box.maxY;
                    x1 = x2 = x3 = x4 = (float) box.minX;

                    minU = (int) (box.minZ * 16);
                    maxU = (int) (box.maxZ * 16);
                    minV = 16 - (int) (box.maxY * 16);
                    maxV = 16 - (int) (box.minY * 16);
                    break;
                case EAST:
                    z1 = z2 = (float) box.minZ;
                    z3 = z4 = (float) box.maxZ;
                    y1 = y4 = (float) box.minY;
                    y2 = y3 = (float) box.maxY;
                    x1 = x2 = x3 = x4 = (float) box.maxX;

                    minU = 16 - (int) (box.maxZ * 16);
                    maxU = 16 - (int) (box.minZ * 16);
                    minV = 16 - (int) (box.maxY * 16);
                    maxV = 16 - (int) (box.minY * 16);
                    break;
            }

            packednormal = calculatePackedNormal(x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4);

            // give our item maximum lighting
            final int BLOCK_LIGHT = 15;
            final int SKY_LIGHT = 15;
            int lightMapValue = LightTexture.pack(BLOCK_LIGHT, SKY_LIGHT);
            TextureAtlasSprite sprite = sprites.length == 1 ? sprites[0] : sprites[direction.ordinal()];

            int[] vertexData1 = vertexToInts(x1, y1, z1, flippedColor.getRGB(), sprite, maxU, maxV, lightMapValue, packednormal);
            int[] vertexData2 = vertexToInts(x2, y2, z2, flippedColor.getRGB(), sprite, maxU, minV, lightMapValue, packednormal);
            int[] vertexData3 = vertexToInts(x3, y3, z3, flippedColor.getRGB(), sprite, minU, minV, lightMapValue, packednormal);
            int[] vertexData4 = vertexToInts(x4, y4, z4, flippedColor.getRGB(), sprite, minU, maxV, lightMapValue, packednormal);
            int[] vertexDataAll = Ints.concat(vertexData1, vertexData2, vertexData3, vertexData4);
            final boolean APPLY_DIFFUSE_LIGHTING = true;
            quads.add(new BakedQuad(vertexDataAll, 0, direction, sprite, APPLY_DIFFUSE_LIGHTING));
        });
        return quads;
    }

    /**
     * Converts the vertex information to the int array format expected by BakedQuads.  Useful if you don't know
     * in advance what it should be.
     *
     * @param x             x coordinate
     * @param y             y coordinate
     * @param z             z coordinate
     * @param color         RGBA colour format - white for no effect, non-white to tint the face with the specified colour
     * @param texture       the texture to use for the face
     * @param u             u-coordinate of the texture (0 - 16) corresponding to [x,y,z]
     * @param v             v-coordinate of the texture (0 - 16) corresponding to [x,y,z]
     * @param lightmapvalue the blocklight+skylight packed light map value (generally: set this to maximum for items)
     *                      http://greyminecraftcoder.blogspot.com/2020/04/lighting-1144.html
     * @param normal        the packed representation of the normal vector, see calculatePackedNormal().  Used for lighting item.
     * @return Vertex information converted into an int array
     */
    private static int[] vertexToInts(float x, float y, float z, int color, TextureAtlasSprite texture, float u, float v, int lightmapvalue, int normal) {
        // based on FaceBakery::storeVertexData and FaceBakery::fillVertexData

//        final int DUMMY_LIGHTMAP_VALUE = 0xffff;

        return new int[]{
                Float.floatToRawIntBits(x),
                Float.floatToRawIntBits(y),
                Float.floatToRawIntBits(z),
                color,
                Float.floatToRawIntBits(texture.getU(u)),
                Float.floatToRawIntBits(texture.getV(v)),
                lightmapvalue,
                normal
        };
    }

    /**
     * Calculate the normal vector based on four input coordinates
     * Follows minecraft convention that the coordinates are given in anticlockwise direction from the point of view of
     * someone looking at the front of the face
     * assumes that the quad is coplanar but should produce a 'reasonable' answer even if not.
     *
     * @return the packed normal, ZZYYXX
     */
    private static int calculatePackedNormal(
            float x1, float y1, float z1,
            float x2, float y2, float z2,
            float x3, float y3, float z3,
            float x4, float y4, float z4) {

        float xp = x4 - x2;
        float yp = y4 - y2;
        float zp = z4 - z2;

        float xq = x3 - x1;
        float yq = y3 - y1;
        float zq = z3 - z1;

        //Cross Product
        float xn = yq * zp - zq * yp;
        float yn = zq * xp - xq * zp;
        float zn = xq * yp - yq * xp;

        //Normalize
        float norm = (float) Math.sqrt(xn * xn + yn * yn + zn * zn);
        final float SMALL_LENGTH = 1.0E-4F;  //Vec3d.normalise() uses this
        if (norm < SMALL_LENGTH) norm = 1.0F;  // protect against degenerate quad

        norm = 1.0F / norm;
        xn *= norm;
        yn *= norm;
        zn *= norm;

        int x = ((byte) (xn * 127)) & 0xFF;
        int y = ((byte) (yn * 127)) & 0xFF;
        int z = ((byte) (zn * 127)) & 0xFF;
        return x | (y << 0x08) | (z << 0x10);
    }
}
