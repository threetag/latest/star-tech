package com.nic.st.util;

import com.nic.st.StarTech;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.AtlasTexture;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.awt.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VoxelEntityModelUtils {

    public enum ModelPart {
        HEAD(Block.box(4, 4, 4, 12, 12, 12)),
        CHEST(Block.box(4, 2, 6, 12, 14, 10)),
        CHEST_SMALL(Block.box(4, 2, 6, 12, 14, 10)),
        LEFT_ARM(Block.box(6, 2, 6, 10, 14, 10)),
        RIGHT_ARM(Block.box(6, 2, 6, 10, 14, 10)),
        LEFT_ARM_SMALL(Block.box(7, 2, 6, 10, 14, 10)),
        RIGHT_ARM_SMALL(Block.box(7, 2, 6, 10, 14, 10)),
        LEFT_LEG(Block.box(6, 2, 6, 10, 14, 10)),
        RIGHT_LEG(Block.box(6, 2, 6, 10, 14, 10)),
        HORIZONTAL_ARM(VoxelShapes.join(Block.box(6, 3, 13, 10, 7, 16), Block.box(7, 3, 14, 9, 7, 16), IBooleanFunction.ONLY_FIRST));
        VoxelShape shape;

        ModelPart(VoxelShape shape) {
            this.shape = shape;
        }
    }

    public static void addModelPart(ModelPart part, List<VoxelShapeData> to) {
        to.add(new VoxelShapeData(part.shape, part.name(), Color.WHITE));
    }

    @Mod.EventBusSubscriber(modid = StarTech.MODID, value = Dist.CLIENT, bus = Mod.EventBusSubscriber.Bus.MOD)
    @OnlyIn(Dist.CLIENT)
    public static class TextureHandler {
        public static final Map<ModelPart, ResourceLocation[]> MODEL_PART_TEXTURES = new HashMap<>();

        static {
            for (ModelPart modelPart : ModelPart.values()) {
                ResourceLocation[] rLs = new ResourceLocation[6];
                for (Direction direction : Direction.values())
                    rLs[direction.ordinal()] = new ResourceLocation(StarTech.MODID, "modelparts/" + modelPart.name().toLowerCase() + "/" + direction.name().toLowerCase());
                MODEL_PART_TEXTURES.put(modelPart, rLs);
            }
        }

        @SubscribeEvent
        public static void onTextureStitchEvent(TextureStitchEvent.Pre event) {
            if (event.getMap().location() == AtlasTexture.LOCATION_BLOCKS) {
                MODEL_PART_TEXTURES.forEach((modelPart, resourceLocations) -> {
                    for (ResourceLocation resourceLocation : resourceLocations) {
                        event.addSprite(resourceLocation);
                    }
                });
            }
        }

        public static TextureAtlasSprite[] getTextures(ModelPart modelPart) {
            AtlasTexture blocksTexture = ModelLoader.instance().getSpriteMap().getAtlas(AtlasTexture.LOCATION_BLOCKS);
            return Arrays.stream(MODEL_PART_TEXTURES.get(modelPart)).map(blocksTexture::getSprite).toArray(TextureAtlasSprite[]::new);
        }
    }
}
